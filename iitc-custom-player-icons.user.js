// ==UserScript==
// @id             iitc-custom-player-icons
// @name           IITC Custom Player Icons
// @category       Tweaks
// @version        2.07
// @description    This plugin changes player tracker icons
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-custom-player-icons.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-custom-player-icons.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==
function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.customPlayerIcons = function() {};

    window.plugin.customPlayerIcons.setupHook = function() {
        if (window.plugin.playerTracker === undefined) {
            console.log("This plugin requires player tracker");
            return;
        }
        addHook('publicChatDataAvailable', window.plugin.customPlayerIcons.setCustomIcons);
    }

    window.plugin.customPlayerIcons.setup = function() {
        // Ensure this hook gets run after player tracker's
        if (window.iitcLoaded) {
            window.plugin.customPlayerIcons.setupHook();
        } else {
            addHook('iitcLoaded', window.plugin.customPlayerIcons.setupHook);
        }
        window.plugin.customPlayerIcons.playerIconMap = {
            "130spectre": "130spectre.png",
            "adikt": "cry.png",
            "alienara": "bluealien.png",
            "alienparanoia": "alienparanoia.png",
            "alnilamb": "alnilamb.png",
            "arcrider": "blue_lightning.png",
            "bagg10": "ball.png",
            "bigmonkey": "greenmonkey.png",
            "biscuitlady": "biscuit.png",
            "blitzenX": "horse.png",
            "buffymaster": "stake.png",
            "chi11ingsi1ence": "megaphone.png",
            "counthackular": "patrick.jpg",
            "crashghost": "crashghost.png",
            "daemonblade": "greenblade.png",
            "donnerX": "horse2.png",
            "draconemignis": "dragon.png",
            "driftkingn1z": "car.png",
            "dysanius": "dice.png",
            "eknz": "snake.png",
            "ellorah": "greenpuppy.png",
//            "epicpandemonium": "blue_panda.png",
            "endlesssleep": "sleep.png",
            "eoink": "pig.png",
            "flame83": "flame.png",
            "frogginz": "green_frog.png",
            "grogyan": "penguin.png",
            "harbourboy": "anchor.png",
            "j4vawarrior": "j4vawarrior.png",
            "juicenz": "juice.png",
            "kaiakl": "star.png",
            "kirdog": "croc.png",
            "kyhwana": "leopard.png",
            "lamb2slaughter": "lts.png",
            "madrigale": "nyan.png",
            "maelan": "cat.png",
            "majiste": "fdl.png",
            "mmmpld": "angel.png",
            "monkeebiz": "monkey.png",
            "myfrankandbeans": "blueshit.png",
            "monsterfishtank": "green_fish.png",
            "navigator01": "greencar.png",
            "neonninja": "blade.png",
            "nikig": "alien.png",
            "noelho": "microscope.png",
            "nuclearangel": "warn.png",
            "nzicequeen": "snowflake.png",
            "nzleagle": "bird.png",
            "orangerice": "rice.png",
            "papabearnz": "bear.png",
            "radiantone": "sun.png",
            "ray457": "green_lightning.png",
            "royalt": "princess.png",
            "shards": "shards.jpg",
            "solandri": "tree.png",
            "spikeylemon": "lemon.png",
            "stillstealth": "stillstealth.png",
            "tamboria": "beer.png",
            "teikaroa": "snowboarder.png",
            "teixobactin": "sick.png",
            "tinnituss": "ear.png",
            "troubledemon": "demon.png",
            "troubleshootnz": "bluegun.png",
            "themotherload": "lady.png",
            "unclebulgaria": "bulgaria.png",
            "zifzone": "turtle.png",
        };
        window.plugin.customPlayerIcons.playerGplusMap = {"aduh":"101779194503855937401","howrudebinksy":"104509569621026116711","kiwisi1":"110367778439167170608","klaphat":"107894994466307591606","gaim90":"105859895856415770187","peterzhanchen":"114568992018051808325","__ada__":"113442350459887977688","aarad":"111806244754936351366","bluffboy":"113682980461605745599","adigitydog33":"111948468507684040566","abbeyface":"117528501205605044274","myfrankandbeans":"102780797762317940089","cryptocrystalin":"106228068997236231141","totum":"101087083226739624698","al1882":"113160701993229734936","dehuyu":"113695015942131253284","albau":"111227353029936288539","roamingkiwi":"114748955585487299570","mrsgliss":"108382400347858348175","zifzone":"108105750482120219562","dodgyduck":"114864804219225456059","krynncl":"109476323281342826239","surfking13":"104080066836962352402","dotn3t":"109037277833337977044","madhatterx33":"101694918298269044249","e45y":"107356983982747179074","surrogatestar":"102383404022259262495","atomixandie":"112810379103972605731","stenberg":"102904909014156326567","narff":"102022214438230308417","andrewb":"110745349072669684877","smaug64":"101602962759474625382","truenoae86":"102658802973104900497","bushworker":"114552796258834373418","bigwinks":"110068529825469703408","skibsinator":"106318742038886267678","anthonysilby":"105859946715419388636","antaxx11":"116279362452016800713","antoninks":"102618102704409927121","aazur":"114216768241387656740","letiteuf55":"110619628141723434464","n0obsaib0t":"116141775757482166834","specksmith":"106523020572306343633","pandeux":"106148774603385766134","pr0noe":"104124561029649214082","beeasaur":"103012075923260499856","cyberbs ":"118304078345830368021","biskyree":"116301573532341254477","djfireboy":"102406815158404091534","titan2303":"101524028126516273692","gayblaknmarried":"100460996427913634023","glorbalmighty":"116336429358327671686","emperorcalibula":"105494195018640045219","hispain":"110381178637718841994","cammerschooner":"103837883511119676200","nurfnz":"108105715510264060817","carlthulhu":"116644211131317457859","stillstealth":"113952694317022441569","carlosconnery":"104597830758045585306","thecarsonator":"112101891121280004119","kaiakl":"101254928730249183517","kiruseki":"104791170260231426520","dadadapanda":"115548727155359165092","chungq":"116326572675328977501","offlorian":"105874719051143408424","mrchris021":"106557155919854574998","hekills":"112280134188986567632","khrious":"116215256291828014023","uninspired":"100195832940694453269","jack0fhearts":"110602123955193931251","emojisweeper":"104178729694994695707","stormyclouds":"105202344667467192435","scrabboracle":"107597323962944043918","zeebobnz":"110452105485197901516","nitikaripley":"100951171456160847817","clarkjkent":"102578305931113329672","sirclemmie":"114040240304265953299","euripidies":"100242742262623413590","conanf007":"108123629192005828462","cordie75":"110425665266313678546","corellus":"115089791176877527994","mrblaz":"100550379901208135729","deusesdaddy":"114081490014474524164","xchaindogx":"100298594632896859189","rxz52":"117940863259573895924","gliss":"111774701236516425644","anshultz":"114443350301221018947","troubleshooternz":"112601886461902011605","cwanand":"101602150359192576362","shaun":"114950458323312768647","j4vawarrior":"106431002976173247622","thunper":"104876063394731934971","boosskat":"112876758626994017369","nz34gle":"108890562160813821057","gimeurshoes":"104166556491584197868","astrodamo":"101110113247885097384","blackmdk":"108989086447917980860","danimalx":"106041543960269901176","chlorcyan":"107904927870020208318","alienara":"105481679183888199198","bigpapahunter":"116455753848394098761","dangom":"114044772991928294467","mechadee":"118372128509686913849","pinolicious":"109086293516673001001","squeej9":"113159723460318549610","ijarvisgps":"118001486419630859135","arcrider":"100219114665875458883","nightmarekid":"104860010087248635452","daxnz":"116657636954532029925","geosaffer":"114874914877161146784","tatvayodha":"103702197452861628593","deechiu":"112592375999642139064","dbene":"106805561675834895714","kumetz":"118123235478177264122","tankgirlnz":"104260459873509882643","tgxn":"114280563195159868184","domstersch":"109043143134428032752","jexia":"106712126894898550732","westwardwind":"115317527137318160571","drunkencyclist":"105388600376922092312","dijitr":"110933577226995178473","skulduggery4":"114456494416200798549","ambermeru22":"112234143503112570608","reddean":"107784026263431699831","lyssaodr":"104100385595613902169","asued":"106120406517171907650","arealrarewhale":"112673190169091630610","atheistbot":"100593119777982469532","iktiv":"113956851214515335734","wak2":"114358838024168586294","cardboardroof":"103762846452490707740","ilwenn":"106215636636324422458","leelomay":"102750510658977602457","mythren":"102385496890609007972","egreeter":"108710631307927820481","earok3d":"108982294274444463145","eknz":"113783731549019347823","tinnituss":"104912815968582234118","irfnf":"106556947534840821425","m3anmachin3":"109423096924867560571","teixobactin":"107002687411463225776","norshk":"118402592111903313705","flame83":"115610572784784996755","reverendwillyg":"106590850412855128237","3clectic":"118313753665092692828","shikaisensz":"113516656073033838081","thisprettything":"115361671451128318411","nzicequeen":"102269123767733164126","gallifreyanp":"103378594408707355744","totalgaz":"108457061256411786104","castoboymanunu":"101468941607655234922","grogyan":"105836408609376826505","harbourboy":"114887540573937028264","xgeoffx":"104286103171306821513","hyro232":"113108048151701172001","goldenrivett":"113248341464359116674","71dark0":"115655323378003354950","snunk":"106769309091659658287","1c3cub3":"106715127077658699109","geeebe":"115973069367930323764","schylla":"117113698211558351327","vangohg":"108836958545265808276","mattmonkey":"106047589958302644203","riskky":"105768841369199466730","melonie":"107419661461535569940","greenbaypacker":"112090874266939016652","greenbaypackers":"103931005850920840698","kornoor":"109534855192110897964","anotherwalrus":"118134869019865511019","passioflora":"110229416577141447657","iluzqx":"107371307618587308378","crimsonspecter":"105654673039844359501","quartzi":"116721929178146175084","lapislazzuli8":"110154397869247209490","tieke":"118198101772654189626","jmax":"106709082179634788936","kampongninja":"116134645047051557762","xxvangarxx":"105018755133792307738","rushden":"104952135121651029955","jdflute":"117418020412749842512","jimsug":"109150293935711922125","aramgutang":"116585940658613166760","smellykaka":"113778299134385895320","thelionkid":"105896002332564388051","jasp02":"115500133178181834200","o666d":"112881453732011464347","belgatherial":"101777819411757271912","chocolatepool":"113273656206546358298","jafanz":"112189757420544409652","blueberries":"109210461557146060994","dermastor":"112664937450344176892","jess474":"116658992368262168477","chloella":"108654643590963940219","goodmorningbaby":"118214931844951120325","jess1er":"108759399556620652885","purplethumbs":"116703265736581045123","bobbychick":"115847033227822827459","draxainth":"105161279159374028346","filthyjoh":"105056100936670485572","jr2026":"107537179485431473938","orangerice":"107611838174422199257","asowneryt":"113046094393296080371","teikaroa":"100385316916278734161","subjonno":"114619389083472825132","capinneemo":"108017917898985729469","jorange":"114179784533866054401","jordynbriearn":"100053722742448742894","ancientmonolith":"117584701827254673861","kamsir":"105907902643828405881","1joshcnz1":"105964809039715614280","jarbury":"102394735415824167263","rarapanda":"116308205338814259926","problex":"109254940284164525192","kupojaycie":"104266820805864146910","kangourouille":"104503722436741359609","seriousjumprole":"114807524897339389990","d0tcould":"117329945538630945223","orangecushion":"112472758825929633050","mackiwi":"103021416102363378673","kanerossnz":"114509966979467873885","nytstar":"115588259364704460978","iiq374":"114737480843242797812","xdaxllamax":"108122656052828705878","kayvv":"117973595380499699876","klyc":"114289474416322746114","projectgman":"100093372170929080318","stealthvixen":"109104425308341607536","sidobelei":"111747885030874098285","kirdog":"109379168584066496154","vesta":"111998222324985037117","randomnuf":"111782083042532938003","hcoastguard":"113497772274994834994","howgozit":"101597107600203121246","gamefacewest":"102740576428715241013","pingpong1908":"114469834474839130870","synergica":"106234563205274378897","valorofmarton":"116492555708450848907","lostinstash":"118111040985931508646","tuzimoe":"100338164705889888180","r2d1":"104149123373055112964","llenlleawg2":"102701297272700661741","lisauco":"111238482946621519078","seanutnz":"111992765646946092203","doublelmc":"116249383879190935586","mythbrion":"108001178366337016823","bikelady":"112620198321871020806","prettyperverse ":"118353226277805637739","losic9h13no3":"104421573873210605888","thisgreatthing":"114061891253213838503","clogkiwi":"103661250337275440888","oyle":"117959928797538690134","aviswindsor":"110037062128545206658","chartreus":"113106865212662428919","kurenaiexe":"103372477944729143648","maksteel":"113372287333819976406","simurgh":"112746735518502184463","flaw1ess":"117684646629010027564","demerk":"114757667489521862609","dev0x00":"102831856259803894085","karm3l":"111601234636216030639","markpnb":"110590479799672597228","themartinator":"114357982020310739587","smarytie":"116655628757822065833","matcat":"105192433664107896932","captaincook187":"103016045514898847981","nzgunslinger ":"112872876780099966021","azullume":"114821195029938411330","beefcake5k":"111984839588409789238","crowbarnz":"110530943531343890310","crowbar214":"115032094315469305618","stompouth8":"100069427177257338315","dartignon":"115043921429995197052","k4rmachameleon":"107133056137380788055","d1pstk":"100805274262633409022","sevynaharis":"117934787351295652051","milkytaste":"111762786952754278264","stringtheorist":"106006377934802434122","titchtitchtitch":"106579963893930262915","johnnapalm":"107944930593112077002","crawlingsine63":"101828489584809993337","seavamp69":"118132052635401035554","resecretweapon":"104508317854256268256","secrektweapon":"113652363653606483139","6fingeredman":"113557013525280403363","mishkin":"101164374779290131774","54moira":"110096075816797235888","msecrektweapon":"110954055768995580820","muzzakiwi":"118036484180032513857","ngawaiata":"117942807522411148969","ghostcotch":"111289527974463925342","nathefresh":"109873162442093741464","thissux1":"103046307215945335263","lunicksilver":"106895487811485063565","buffymaster":"107737782926748581884","vampnik":"110616347485266785078","ninnish":"114037205937835747625","sekainohametsu":"111456566572454238050","chaos1011":"114895414823057384720","williejoe":"105102537061310894956","trololo111":"105330958450794948252","princessscrub":"113886712832051763790","pgaspari":"101381345182445589962","paraselene":"111949735390960401225","pepperedpig":"100209366324342783001","counthackular":"100456472834745261620","traceurfer":"114335034973113622865","ooojanz":"104456407213504754296","secretelder":"112106364035278499703","ntrdr":"113445167596928403831","orklander":"108155823014899983081","linxnz":"105334134276456630939","c4ncercell":"114710964184432731095","mungo666":"101735233659527248240","lancethermal":"104245475149058767975","husaberg390cc":"108672765584098709163","ptchernegovski":"118245242631233514202","risktastic":"109023038455080004486","pgdestroy3r":"102607618512640600611","philipkil":"111244867779959424701","roypysco":"109247647401402956957","prestybear":"100615924460186901964","tnslppbntso":"112423649086122144812","clanvanhellsing":"103511949723586191610","rjkashyap":"113072962046081627829","radiantone":"109240644111829592418","sting1606":"113136568567098052773","accaber":"109155182370225459224","alienparanoia":"105375670602780938076","kirjava":"109334315678977274151","steelrhino":"103741555160261732771","icaio":"105657981259358228985","raim669":"104278799107358777308","arandomoracle":"111534391706371250156","choqbhuntest":"105441195638113101289","choqbhun":"115661607639787057740","scythewing":"115124758907333196146","nzmkey":"107176204640191915326","razorfoot":"117594054535630964776","rhazgul":"102367205273058385706","danob1":"100711578312968338254","lisit":"104995160216154462592","rogy8":"103019681287336299748","bc4bear":"102446887832276881198","azrael8419":"115393903945515284985","solandri":"105954141147391286931","mousemasteross":"110514242374963901060","bradleigh":"117743140726032897215","killerbok":"102453239377987141898","sspp0000xx":"110014573491646727828","reaperblitz":"115314010715816603153","ariesbt":"110642633516437542589","yakkitty":"113888634824544013370","":"101500159422782038705","petrichortap":"109611887194968302437","razniator":"107891173990691314148","tonystarx":"105929384948584043965","thesandy":"110730274956163547134","fussballkatze":"105887110185377464102","phaera":"108445195530028104955","sarahkatemcc":"100118483387579630925","slamsam01":"103823475627185345801","shessassy":"100445315253935143532","sashasmith":"105779692080677421902","fasce":"113888010421344630980","iisazzuii":"102202739689218412414","scr1b3":"108288352186977672074","redpandanz":"101980889493368792945","ghanjalf":"113028743118081317708","cybersa":"101700751548928584140","kiwiijuggalo":"111017874304075948118","elshawno":"115021353677908479771","tamboria":"117985740928520565355","shisca":"109938260617618091286","silkenz":"116044066272751690270","saimonelia":"115213736017440351589","tisio2":"100259425214039355971","spjv01":"105849719219551360276","temporalsage":"108277919961520498015","sinny1":"110279369256240899000","quames":"104538825890883572892","laracroftcba":"115087711979256108598","landmnz":"107363543026329555218","gingernater":"101573533565052211156","bomberharrisili":"109738328520520622885","sincrosis":"109380333578713156825","flobbers":"100553520415010808768","dispersedwolf":"107723205738042835278","swill48":"110767208602244596398","g1lakc":"104956339516833352665","suratt":"115947597686797498362","kikorangi":"118368651774383300088","metavercer":"109530631716011108176","gecko5":"107383345043265299785","kingkru":"104609427252672010122","balalala263":"113233585774455634146","tastyorange":"116223957666736122431","dronedkiwi":"104155493660297422360","tharrin":"104283997841036322121","chairmanyau":"114491730079394873513","truekiwi":"100719619077310195974","t0omz":"101434900317687574589","stallionnz":"103401768630296314810","tindog2":"111627942399801866936","pimpx":"102307284029807259902","arctorius679":"105588457102088152872","fursdy":"113481700283274660666","accylad":"113330165655168544434","f1ndme2":"103321707505283075222","topshelfdcsa":"111187374994205454578","a5tr1x":"108268211446237152107","hio77":"108102369770530244898","madvee":"118244861791222476607","vrosalia":"100289792644094604190","nvov":"111925941848805970042","xaedosx":"110567617071436954383","blitzenx":"114124577543556602616","bigtohora":"113288860271737030594","surgamiv":"102193069157024762656","hulijing":"117162998648105482034","smashpippi":"101818413013836283679","yardiff":"100864621614299609586","rogerwillko":"106252400483103368067","endervw":"116652288134159906649","volcano118":"117841670885226344998","mobilete":"103523610672892986638","animusclone":"110663212710905199509","tyxsok":"106241541869038832448","epicpandemonium":"100517064253738453308","seonyi":"102038319666690004141","xenderlordx":"109732073580639336026","imuza":"115037498929336446704","zedberstrix":"112876652970587988958"};
        window.plugin.customPlayerIcons.playerGplusCache = {};
    };

    window.plugin.customPlayerIcons.setCustomIcons = function() {
        $.each(window.oms.markers, function(i,marker) {
            var playerName;
            if (marker.options.desc && marker.options.desc.firstElementChild && marker.options.desc.firstElementChild.textContent) {
                playerName = marker.options.desc.firstElementChild.textContent;
            } else if (marker.options.title) {
                playerName = marker.options.title.substring(0, marker.options.title.indexOf(','));
            }
            if (playerName) {
                console.log(playerName + " detected");
                var icon = window.plugin.customPlayerIcons.playerIconMap[playerName.toLowerCase()];
                if (icon) {
                    var customIcon = L.icon({
                        iconUrl: "https://gitlab.com/neonninja/iitc_plugins/raw/master/emoji/" + icon
                    });
                    marker.setIcon(customIcon);
                    console.log("set custom icon for " + playerName);
                } else {
                    var g = window.plugin.customPlayerIcons.playerGplusMap[playerName.toLowerCase()];
                    if (g) {
                      if (window.plugin.customPlayerIcons.playerGplusCache[g]) {
                        var iconUrl = window.plugin.customPlayerIcons.playerGplusCache[g];
                        var customIcon = L.icon({
                            iconUrl: iconUrl
                        });
                        marker.setIcon(customIcon);
                        console.log("set custom icon for " + playerName + " from the g+ cache");
                      } else {
                        var url = 'https://people.googleapis.com/v1/people/' + g + '?personFields=photos&key=AIzaSyD-PrrqSgPjL1GWm8-p1go8Ty-FYV5HBEI';
                        $.getJSON(url, function(d) {
                            var iconUrl = d.photos[0].url;
                            window.plugin.customPlayerIcons.playerGplusCache[g] = iconUrl;
                            var customIcon = L.icon({
                                iconUrl: iconUrl
                            });
                            marker.setIcon(customIcon);
                            console.log("set custom icon for " + playerName + " from their g+");
                        });
                      }
                    }
                }
            }
        });
    };

    var setup = window.plugin.customPlayerIcons.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
};
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
