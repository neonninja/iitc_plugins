// ==UserScript==
// @id             iitc-drawtools-idl-fix
// @name           IITC Draw Tools IDL fix
// @category       Tweaks
// @version        1.03
// @description    This plugin attempts to harden drawn items against the IDL bug. Also fixes links and fields across the IDL.
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-drawtools-idl-fix.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-drawtools-idl-fix.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.idlfix = function() {};

    window.plugin.idlfix.setup = function() {
        $('#toolbox').append('<a tabindex="0" onclick="window.plugin.idlfix.run()">Fix IDL</a>');
        addHook("fieldAdded", function(poly) {
            var ent = $.extend(true, {}, poly.field.options.ent);
            if (ent.idlfix) return;
            ent.idlfix = true;
            for (var i in ent[2][2]) {
                var gll = ent[2][2][i];
                if (gll[2] < 0) {
                    ent[2][2][i][2] += 360000000;
                } else if (gll[2] > 0) {
                    ent[2][2][i][2] -= 360000000;
                }
            }
            mapDataRequest.render.createFieldEntity(ent);
        });
        addHook("linkAdded", function(poly) {
            var ent = $.extend(true, {}, poly.link.options.ent);
            if (ent.idlfix) return;
            ent.idlfix = true;
            var lng1 = ent[2][4];
            var lng2 = ent[2][7];
            if (lng1 < 0) {
                ent[2][4] += 360000000;
            } else if (lng1 > 0) {
                ent[2][4] -= 360000000;
            }
            if (lng2 < 0) {
                ent[2][7] += 360000000;
            } else if (lng2 > 0) {
                ent[2][7] -= 360000000;
            }
            mapDataRequest.render.createLinkEntity(ent);
        });
    };

    window.plugin.idlfix.run = function() {
        var existing = JSON.parse(localStorage['plugin-draw-tools-layer']);
        var rev = [];
        for (var item of existing) {
            var revItem = $.extend(true, {}, item); // Deep copy
            if (revItem.latLngs) { // polyline or polygon
                for (var latlng of revItem.latLngs) {
                    if (latlng.lng < 0) {
                        latlng.lng += 360;
                    } else if (latlng.lng > 0) {
                        latlng.lng -= 360;
                    }
                }
            } else if (revItem.latLng) { // circle
                if (revItem.latLng.lng < 0) {
                    revItem.latLng.lng += 360;
                } else if (revItem.latLng.lng > 0) {
                    revItem.latLng.lng -= 360;
                }
            }
            rev.push(revItem);
        }
        window.plugin.drawTools.import(rev);
    }

    var setup = window.plugin.idlfix.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


