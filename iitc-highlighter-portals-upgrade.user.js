// ==UserScript==
// @id             iitc-highlighter-portals-upgrade
// @name           IITC Highlight upgradable portals
// @category       Highlighter
// @version        1.01
// @description    This plugin highlights portals that you can upgrade
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-highlighter-portals-upgrade.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-highlighter-portals-upgrade.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.portalHighlighterUpgradable = function() {};

    window.plugin.portalHighlighterUpgradable.colorLevel = function(data) {
        var g = data.portal.options.guid;
        var d = {'resonators': resonators[g]};
        if (!d.resonators) return;
        var t = data.portal.options.data.team;
        if (t == 'R') d.team = 'RESISTANCE';
        if (t == 'E') d.team = 'ENLIGHTENED';
        var current_level = getPortalLevel(d);
        var potential_level = window.potentialPortalLevel(d);
        var player_level = PLAYER.level;
        var opacity = .7;

        if( potential_level > current_level) {
            potential_level = Math.floor(potential_level);
            current_level = Math.floor(current_level);
            //console.log(potential_level + '>' + current_level);
            var color = 'yellow';
            if(potential_level > current_level) {
                color = 'red';
            }
            data.portal.setStyle({fillColor: color, fillOpacity: opacity});

        }
    };

    var setup =  function() {
        window.addPortalHighlighter('Upgradable', window.plugin.portalHighlighterUpgradable.colorLevel);
        window.resonators = {};
        addHook('portalDetailLoaded', function(d) {
            resonators[d.guid] = d.details.resonators;
        });
        addHook('portalAdded', function(d) {
            var guid = d.portal.options.guid;
            if (d.portal.options.team == TEAM_RES && d.portal.options.level == 7) {
                window.portalDetail.request(guid);
            }
        });
    };

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


