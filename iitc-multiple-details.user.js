// ==UserScript==
// @id             iitc-multiple-details
// @name           IITC Multiple Details
// @category       Tweaks
// @version        1.04
// @description    This plugin displays multiple portal details at once
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-multiple-details.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-multiple-details.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.mpd = function() {};

    window.plugin.mpd.renderLinkedPortals = function (guid) {

        var portalLinks = getPortalLinks(guid);
        var length = portalLinks.in.length + portalLinks.out.length;

        var c = 1;

        var container = $('<div>',{id:'showLinkedPortalContainer'});

        function renderLinkedPortal(linkGuid) {
            if(c > 16) return;

            var key = this; // passed by Array.prototype.forEach
            var link = window.links[linkGuid].options.data;
            var guid = link[key + 'Guid'];
            var lat = link[key + 'LatE6']/1E6;
            var lng = link[key + 'LngE6']/1E6;

            var length = L.latLng(link.oLatE6/1E6, link.oLngE6/1E6).distanceTo([link.dLatE6/1E6, link.dLngE6/1E6]);
            var lengthFull = digits(Math.round(length)) + 'm';
            var lengthShort = length < 100000 ? lengthFull : digits(Math.round(length/1000)) + 'km';

            var div = $('<div>').addClass('showLinkedPortalLink showLinkedPortalLink' + c + (key=='d' ? ' outgoing' : ' incoming'));

            var title;

            var data = (portals[guid] && portals[guid].options.data) || portalDetail.get(guid) || null;
            if(data && data.title) {
                title = data.title;
                div.append($('<img/>').attr({
                    'src': fixPortalImageUrl(data.image),
                    'class': 'minImg',
                    'alt': title,
                }));
            } else {
                title = 'Go to portal';
                div
                    .addClass('outOfRange')
                    .append($('<span/>')
                            .html('Portal not loaded.<br>' + lengthShort));
            }

            div
                .attr({
                'data-guid': guid,
                'data-lat': lat,
                'data-lng': lng,
                'title': $('<div/>')
                .append($('<strong/>').text(title))
                .append($('<br/>'))
                .append($('<span/>').text(key=='d' ? '↴ outgoing link' : '↳ incoming link'))
                .append($('<br/>'))
                .append($('<span/>').html(lengthFull))
                .html(),
            })
                .appendTo(container);

            c++;
        }

        portalLinks.out.forEach(renderLinkedPortal, 'd');
        portalLinks.in.forEach(renderLinkedPortal, 'o');
        
        return container.html();
    };

    window.plugin.mpd.setup = function() {
        console.log("Multiple Details enabled");
        $("<style>").prop("type", "text/css").html("#randdetails td, #resodetails td { overflow: visible }").appendTo("head");
        window.addHook('portalDetailLoaded', function(data) {
            console.log('got portal details',JSON.stringify(data));
            var guid = data.guid;
            var details = data.details;
            var modDetails = details ? '<div class="mods">'+getModDetails(details)+'</div>' : '';
            var miscDetails = details ? getPortalMiscDetails(guid,details) : '';
            var resoDetails = details ? getResonatorDetails(details) : '';
            var title = details.title;
            var level = getPortalLevel(details);
            var img = details.image;
            var html = $("<div></div>").attr('class', TEAM_TO_CSS[teamStringToId(details.team)]).append(

                // help cursor via ".imgpreview img"
                $('<div>')
                .attr({class:'imgpreview', style:"background-image: url('"+img+"')"})
                .append(
                    $('<span>').attr({id:'level',style:'margin-top:15px'}).text(level)
                ),

                modDetails,
                miscDetails,
                resoDetails,
                window.plugin.mpd.renderLinkedPortals(guid)
            );
            if (window.DIALOG_COUNT === 0) {
                window.plugin.mpd.target = window;
                window.plugin.mpd.at = 'left top';
                window.plugin.mpd.first = null;
            } else if (window.DIALOG_COUNT == 6) {
                window.plugin.mpd.target = window.plugin.mpd.first;
                window.plugin.mpd.at = 'left bottom';
            }
            var options = {
                id: 'mpd-'+guid.replace('.',''),
                title: title,
                html: html,
                position: {my: 'left top', at: window.plugin.mpd.at, of: window.plugin.mpd.target, collision: 'fit'},
                buttons: []
            };
            window.plugin.mpd.target = dialog(options).parent();
            if (!window.plugin.mpd.first) window.plugin.mpd.first = window.plugin.mpd.target;
            window.plugin.mpd.at = 'right top';
        });
    };

    var setup = window.plugin.mpd.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


