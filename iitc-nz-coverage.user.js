// ==UserScript==
// @id             iitc-nz-coverage
// @name           IITC: NZ Coverage Overlays
// @category       Map Tiles
// @version        1.05
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-nz-coverage.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-nz-coverage.user.js
// @description    NZ 2D/Voda/Spark coverage overlays for IITC. Also a LINZ aerial imagery layer.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {

    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    // use own namespace for plugin
    window.plugin.nzcoverage = function() {};

    window.plugin.nzcoverage.addLayer = function() {

        layer = L.tileLayer("https://basemaps.linz.govt.nz/v1/tiles/aerial/EPSG:3857/{z}/{x}/{y}.webp?api=d01eyvkkr9erajj4zpeqykezbgn", {
            maxZoom: 23,
            maxNativeZoom: 22,
            attribution: '© <a href="//www.linz.govt.nz/linz-copyright">LINZ CC BY 4.0</a> © <a href="//www.linz.govt.nz/data/linz-data/linz-basemaps/data-attribution">Imagery Basemap contributors</a>'
        });
        layerChooser.addBaseLayer(layer, "LINZ Aerial");

        var endpoints = {
            "Vodafone Coverage 4G": "https://api-proxy.auckland-cer.cloud.edu.au/voda/?https://s1-vodafone.cloud.eaglegis.co.nz/arcgis/rest/services/coverage-maps/Mobile_4G_0_T_990000/MapServer/tile/{z}/{y}/{x}",
            "Vodafone Coverage 4G (700 MHz)": "https://api-proxy.auckland-cer.cloud.edu.au/voda/?https://s1-vodafone.cloud.eaglegis.co.nz/arcgis/rest/services/coverage-maps/Mobile_4GqqExtend_0_T_E60000/MapServer/tile/{z}/{y}/{x}",
            "Vodafone Coverage 3G": "https://api-proxy.auckland-cer.cloud.edu.au/voda/?https://s1-vodafone.cloud.eaglegis.co.nz/arcgis/rest/services/coverage-maps/Mobile_3G_0_T_FF5400/MapServer/tile/{z}/{y}/{x}",
            "Spark Coverage 4G": "https://spark-viewer.wivolo.com/tiles/render?dataset=coverage.4g.1612263600&fmt=png32&nocache=1612474119&index={z}/{x}/{y}",
            "Spark Coverage 3G": "https://spark-viewer.wivolo.com/tiles/render?dataset=coverage.3g850.1608030000&fmt=png32&nocache=1608530632&index={z}/{x}/{y}",
            "2D Coverage 4G": "https://www.2degreesmobile.co.nz/assets/tiles/v30/L18/{z}/{x}/{y}.png",
            "2D Coverage 3G (900 MHz)": "https://www.2degreesmobile.co.nz/assets/tiles/v30/U09/{z}/{x}/{y}.png",
            "2D Coverage 3G": "https://www.2degreesmobile.co.nz/assets/tiles/v30/U21/{z}/{x}/{y}.png",
        };

        var options = {tileSize: 256, opacity: 0.5};

        for (var name in endpoints) {
            var url = endpoints[name];
            var layer = new L.TileLayer(url, options);
            layerChooser.addOverlay(layer, name);
        }

    };

    var setup =  window.plugin.nzcoverage.addLayer;

    ///////////////////////////////////////////////////////////
    // PLUGIN END
    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();

} // wrapper end

// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
