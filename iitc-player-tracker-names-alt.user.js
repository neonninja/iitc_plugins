// ==UserScript==
// @id             iitc-player-tracker-names-alt
// @name           IITC Player Tracker Names
// @category       Tweaks
// @version        1.10
// @description    This plugin displays player names for player tracker
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-player-tracker-names-alt.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-player-tracker-names-alt.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.playerTrackerNames = function() {};

    window.plugin.playerTrackerNames.toggle = function() {
        window.plugin.playerTrackerNames.enabled = !window.plugin.playerTrackerNames.enabled;
        if (window.plugin.playerTrackerNames.enabled) {
            window.plugin.playerTrackerNames.setupHook();
        } else {
            if (plugin.playerTracker) {
                plugin.playerTracker.drawnTracesRes.eachLayer(function(layer) { if (layer.closePopup) layer.closePopup(); });
                plugin.playerTracker.drawnTracesEnl.eachLayer(function(layer) { if (layer.closePopup) layer.closePopup(); });
            }
            if (plugin.advancedPlayerTracker) {
                plugin.advancedPlayerTracker.drawnTracesRes.eachLayer(function(layer) { if (layer.closePopup) layer.closePopup(); });
                plugin.advancedPlayerTracker.drawnTracesEnl.eachLayer(function(layer) { if (layer.closePopup) layer.closePopup(); });
            }
        }
    };

    window.plugin.playerTrackerNames.layerFunction = function(layer) {
        if ($(layer._icon) && layer.bindTooltip && layer.options.desc) {
            console.log(layer);
            var text = layer.options.title;
            layer.bindTooltip(text, {permanent: true}).openTooltip();
            console.log('added label for ' + text);
        }
    };

    window.plugin.playerTrackerNames.setupHook = function() {
        if (!window.plugin.playerTrackerNames.enabled) return;
        window.isTouchDevice = function() { return false; };
        if (plugin.playerTracker) {
            plugin.playerTracker.drawnTracesRes.eachLayer(window.plugin.playerTrackerNames.layerFunction);
            plugin.playerTracker.drawnTracesEnl.eachLayer(window.plugin.playerTrackerNames.layerFunction);
        }
        if (plugin.advancedPlayerTracker) {
            plugin.advancedPlayerTracker.drawnTracesRes.eachLayer(window.plugin.playerTrackerNames.layerFunction);
            plugin.advancedPlayerTracker.drawnTracesEnl.eachLayer(window.plugin.playerTrackerNames.layerFunction);
        }
    };


    window.plugin.playerTrackerNames.setup = function() {
        if (!window.plugin.playerTracker && !window.plugin.advancedPlayerTracker) {
            console.error("This plugin requires player tracker");
            return;
        }
        window.isTouchDevice = function() { return false; };
        window.plugin.playerTrackerNames.enabled = true;
        addHook('publicChatDataAvailable', window.plugin.playerTrackerNames.setupHook);
        $('#toolbox').append('<a tabindex="0" onclick="window.plugin.playerTrackerNames.toggle()">Toggle player names</a>');
    };

    var setup = window.plugin.playerTrackerNames.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
};
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
