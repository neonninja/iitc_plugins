// ==UserScript==
// @id             iitc-player-tracker-names
// @name           IITC Player Tracker Names
// @category       Tweaks
// @version        1.1
// @description    This plugin displays player names for player tracker
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-player-tracker-names.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-player-tracker-names.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==
function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if (typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.playerTrackerNames = function() {};

    window.plugin.playerTrackerNames.setupHook = function() {
        if (window.plugin.playerTracker === undefined) {
            console.log("This plugin requires player tracker");
            return;
        }
        // override setupTooltips so that multiple tooltips can be open at once
        window.setupTooltips = function(element) {
            element = element || $(document);
            element.tooltip({
                content: function() {
                    var title = $(this).attr('title');
                    return window.convertTextToTableMagic(title);
                },
                close: function(event, ui) {
                    setTimeout(function() {
                        $(event.target).tooltip('open');
                    }, 3000);
                },
            });
            element.tooltip('open');
        }
        $(document).tooltip('destroy');
        window.setupTooltips();
        $("<style>").prop("type", "text/css").html(".ui-tooltip {z-index: 1000 !important}").appendTo("head");
        console.log("Player Names enabled");
    }

    window.plugin.playerTrackerNames.setup = function() {
        // Ensure this hook gets run after player tracker's
        if (window.iitcLoaded) {
            window.plugin.playerTrackerNames.setupHook();
        } else {
            addHook('iitcLoaded', window.plugin.playerTrackerNames.setupHook);
        }
    };

    var setup = window.plugin.playerTrackerNames.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if (!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if (window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
};
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
