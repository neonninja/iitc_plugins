// ==UserScript==
// @id             iitc-plugin-dispatch-admin
// @name           IITC plugin: Dispatch Assistance Admin
// @author         Original Author: @ddellspe, updates by @neonninja
// @version        0.5.7
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-plugin-dispatch-admin.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-plugin-dispatch-admin.user.js
// @description    Assist with dispatch.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @require        http://leaflet.github.io/Leaflet.label/leaflet.label.js
// @grant          none
// ==/UserScript==
//
//changelog: v0.2.2 fixed exported .csv output
//           v0.3.0 added back export/import tool(still working on)
//           v0.3.1 export/import fully working and import is now backwards compatiable with older versions
//           v0.3.2 bug fixes
//           v0.4.0 reset of the backend, more data stored locally to allow for easier exchange of data and visibility
//           v0.4.1 added in required spectrum code
//           v0.5.0 Changed this script to the non-builders version, removed auto-cluster, all other functionality remains
//           v0.5.1 Gave some options for the clusters as far as viewing goes and removing portals
//           v0.5.2 Optimised labels
//           v0.5.6 Simplify label css

function wrapper(plugin_info) {
    
    
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    
    // PLUGIN START ////////////////////////////////////////////////////////
    
    // use own namespace for plugin
    window.plugin.dispatchAssist = function() {};
    
    window.plugin.dispatchAssist.NAME_WIDTH = 30;
    window.plugin.dispatchAssist.NAME_HEIGHT = 30;
    
    window.plugin.dispatchAssist.labelLayers = {};
    window.plugin.dispatchAssist.cluster1LabelGroup = null;
    window.plugin.dispatchAssist.cluster2LabelGroup = null;
    window.plugin.dispatchAssist.cluster3LabelGroup = null;
    window.plugin.dispatchAssist.cluster4LabelGroup = null;
    window.plugin.dispatchAssist.clusterXLabelGroup = null;
    
    window.plugin.dispatchAssist.clusterToRemove = null;
    
    window.plugin.dispatchAssist.portalDispatchInfo = {};
    
    window.plugin.dispatchAssist.options = {};
    
    window.plugin.dispatchAssist.getClusterPortals = function() {
        //filter : 0 = All, 1 = Res, 2 = Enl
        var retval = false;
        
        var displayBounds = map.getBounds();
        
        window.plugin.dispatchAssist.clusterPortals = [];
        $.each(window.portals, function(guid, portal) {
            // eliminate offscreen portals (selected, and in padding)
            if(!displayBounds.contains(portal.getLatLng())) return true;
            
            retval = true;
            var d = portal.options.data;
            
            if(d.ornaments.length >= 1) {
                // we know this portal is in the clusters, let's load all the data that's needed now
                var coord = portal.getLatLng();
                
                if(guid in window.plugin.dispatchAssist.portalDispatchInfo) {
                    window.plugin.dispatchAssist.portalDispatchInfo[guid].cluster = d.ornaments[0][2];
                    window.plugin.dispatchAssist.portalDispatchInfo[guid].lat = coord.lat;
                    window.plugin.dispatchAssist.portalDispatchInfo[guid].lng = coord.lng;
                    window.plugin.dispatchAssist.portalDispatchInfo[guid].image = d.image;
                    window.plugin.dispatchAssist.portalDispatchInfo[guid].name = d.title;
                } else {
                    window.plugin.dispatchAssist.portalDispatchInfo[guid] = {
                        'cluster': d.ornaments[0][2],
                        'lat': coord.lat,
                        'lng': coord.lng,
                        'image': d.image,
                        'name': d.title,
                    };
                }
            }
        });
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.getClustersAndDisplayPL = function() {
        window.plugin.dispatchAssist.getClusterPortals();
        window.plugin.dispatchAssist.displayPL();
    };
    
    window.plugin.dispatchAssist.displayPL = function() {
        var html = '';
        
        if(Object.keys(window.plugin.dispatchAssist.portalDispatchInfo).length !== 0) {
            html += window.plugin.dispatchAssist.portalTable();
        } else {
            html = '<table class="noPortals"><tr><td>Nothing to show!</td></tr><tr><td>To Import Portals, look at the Dispatch Opt link in the side</td></tr></table>';
        }
        
        if(window.useAndroidPanes()) {
            $('<div id="dispatchlist" class="mobile">' + html + '</div>').appendTo(document.body);
        } else {
            dialog({
                html: '<div id="dispatchlist">' + html + '</div>',
                dialogClass: 'ui-dialog-dispatchAssist',
                title: 'Portal list: ' + Object.keys(window.plugin.dispatchAssist.portalDispatchInfo).length + ' ' + (Object.keys(window.plugin.dispatchAssist.portalDispatchInfo).length == 1 ? 'portal' : 'portals'),
                id: 'portal-list',
                width: 700,
                buttons: {
                    "Export CSV": window.plugin.dispatchAssist.exportPortalCSV
                }
            });
        }
    };
    
    window.plugin.dispatchAssist.getAllClusters = function(){
        var uniqueClusters = [];
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, portal) {
            if(uniqueClusters.indexOf(portal.cluster) == -1){
                uniqueClusters.push(portal.cluster);
            }
        });
        uniqueClusters = uniqueClusters.sort();
        var html = "<option> -- </option>";
        for(var i = 0 ; i < uniqueClusters.length ; i++){
            html += '<option value="'+uniqueClusters[i]+'">'+uniqueClusters[i]+"</option>";
        }
        return html;
    }
    
    window.plugin.dispatchAssist.removeFromCluster = function(cluster){
        var portalsToRemove = [];
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, portal) {
            if(portal.cluster === cluster){
                portalsToRemove.push(guid);
            }
        });
        for(var i = 0 ; i < portalsToRemove.length ; i++){
            delete window.plugin.dispatchAssist.portalDispatchInfo[portalsToRemove[i]];
        }
        window.plugin.dispatchAssist.savePortalDispatchInfo();
        window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
    }
    
    window.plugin.dispatchAssist.findVolatiles = function() {
        var foundNew = false;
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, portal) {
            var portalDetails = window.portals[guid];
            if(portalDetails && portalDetails.options.data.ornaments.some(function(ornament){return ornament.match(/_v$/)})){
                if(portal.codename === undefined){
                    portal.codename = '*';
                    foundNew = true;
                } else if(portal.codename.charAt(0) !== '*') {
                    portal.codename = '*' + portal.codename;
                    foundNew = true;
                }
            }
        });
        if(foundNew){
            window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        }
    };
    
    window.plugin.dispatchAssist.portalTable = function() {
        window.plugin.dispatchAssist.findVolatiles();
        // save the sortBy/sortOrder/filter
        var clusterPortals = [];
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, portal) {
            var thisPortal = {
                'clusterNumber': portal.cluster,
                'codename': portal.codename,
                'guid': guid,
                'name': portal.name,
                'portalLink': window.plugin.dispatchAssist.getPortalLink(portal, guid)
            };
            clusterPortals.push(thisPortal);
        });
        
        //Array sort
        clusterPortals.sort(function(a, b) {
            var retVal = 0;
            var aComp = a.clusterNumber;
            var bComp = b.clusterNumber;
            var aComp2 = a.codename || '~~';
            var bComp2 = b.codename || '~~';
            
            if(aComp < bComp) {
                retVal = -1;
            } else if(aComp > bComp) {
                retVal = 1;
            } else if(aComp2 < bComp2) {
                retVal = -1;
            } else if(aComp2 > bComp2) {
                retVal = 1;
            } else {
                // equal - compare GUIDs to ensure consistent (but arbitrary) order
                retVal = a.guid < b.guid ? -1 : 1;
            }
            // sortOrder is 1 (normal) or -1 (reversed)
            retVal = retVal * 1;
            return retVal;
        });
        
        var html = '<table class="portals">';
        html += '<tr class="header">';
        html += '<th>Cluster</th>';
        html += '<th>Codename</th>';
        html += '<th>Name (click to see details)</th>';
        html += '<th>Reset Codename</th>';
        html += '</tr>\n';
        
        $.each(clusterPortals, function(ind, portal) {
            var clusterNumber = portal.clusterNumber || '--';
            var codename = portal.codename || '--';
            html += '<tr class="neutral">';
            html += '<td class="portalClusterNumber" data-guid="' + portal.guid + '" data-name="' + portal.name + '">' + clusterNumber + '</td>';
            html += '<td class="portalCodename" data-guid="' + portal.guid + '" data-name="' + portal.name + '">' + codename + '</td>';
            html += '<td class="portalTitle" style="">' + portal.portalLink + '</td>';
            html += '<td class="portalCodenameDelete" data-guid="' + portal.guid + '" data-name="' + portal.name + '">' + codename + '</td></tr>';
        });
        html += '</table>';
        
        return html;
    };
    
    window.plugin.dispatchAssist.exportPortalCSV = function() {
        
        var filename = prompt('Enter Export Filename');
        if(filename) {
            if(filename.indexOf('.csv', filename.length - 4) === -1) {
                filename = filename + '.csv';
            }
        } else {
            filename = 'cluster_portals.csv';
        }
        var data = new Array(Object.keys(window.plugin.dispatchAssist.portalDispatchInfo).length + 1);
        data[0] = ['Cluster', 'Label', 'Name', 'External Link', 'Image URL', 'Latitude', 'Longitude', 'guid'];
        var count = 1;
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, portal) {
            data[count] = ['"' + (portal.cluster || '') + '"',
                           '"' + (portal.codename || '') + '"',
                           '"' + portal.name + '"',
                           '"' + window.plugin.dispatchAssist.getExternalPortalLink(portal, guid) + '"',
                           '"' + portal.image + '"',
                           '"' + portal.lat + '"',
                           '"' + portal.lng + '"',
                           '"' + guid + '"'
                          ];
            count++;
        });
        for(var i = 0; i < data.length; i++) {
            if(data[i] === undefined) {
                data.splice(i, 1);
                i--;
            }
        }
        var csvContent = "data:text/csv;charset=utf-8,";
        
        data.forEach(function(infoArray, index) {
            dataString = infoArray.join(",");
            csvContent += dataString + "\n";
        });
        
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        
        link.href = encodedUri;
        link.target = '_blank';
        link.download = filename;
        document.body.appendChild(link);
        link.click();
    };
    
    window.plugin.dispatchAssist.getPortalLink = function(portal, guid) {
        // portal link - single click: select portal
        //               double click: zoom to and select portal
        //               hover: show address
        // code from getPortalLink function by xelio from iitc: AP List - https://raw.github.com/breunigs/ingress-intel-total-conversion/gh-pages/plugins/ap-list.user.js
        var latlng = [portal.lat, portal.lng].join();
        var jsSingleClick = 'window.renderPortalDetails(\'' + guid + '\');return false';
        var jsDoubleClick = 'window.zoomToAndShowPortal(\'' + guid + '\', [' + latlng + ']);return false';
        var perma = '/intel?latE6=' + portal.lat + '&lngE6=' + portal.lng + '&z=17&pguid=' + guid;
        
        //Use Jquery to create the link, which escape characters in TITLE and ADDRESS of portal
        var a = $('<a>', {
            text: portal.name,
            title: portal.name,
            href: perma,
            onClick: jsSingleClick,
            onDblClick: jsDoubleClick
        })[0].outerHTML;
        
        return a;
    };
    
    window.plugin.dispatchAssist.getExternalPortalLink = function(portal, guid) {
        // Provides the permalink for the portal based on standard links
        var perma = 'http://ingress.com/intel?ll=' + portal.lat + ',' + portal.lng + '&z=17&pll=' + portal.lat + ',' + portal.lng;
        return perma;
    };
    
    window.plugin.dispatchAssist.onPaneChanged = function(pane) {
        if(pane == "plugin-dispatchAssist")
            window.plugin.dispatchAssist.displayPL();
        else
            $("#dispatchAssist").remove();
    };
    
    
    window.plugin.dispatchAssist.setupCSS = function() {
        var html = '.plugin-dispatch-assistant{';
        html += 'color:#FFFFBB;';
        html += 'font-size:12px;line-height:12px;';
        html += 'font-weight:bold;';
        html += 'text-align:center;padding: 2px;';
        html += 'overflow:hidden;';
        html += 'text-shadow:1px 1px #fff,1px -1px #fff,-1px 1px #fff,-1px -1px #fff, 0 0 5px #000;';
        html += 'pointer-events:none;';
        html += '}';
        html += '.leaflet-label{position:absolute;left:-22px;top:5px;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;pointer-events:none;text-shadow: 0 0 0.5em black, 0 0 0.5em black, 0 0 0.5em black;}';
        $("<style>").prop("type", "text/css").html(html).appendTo("head");
    };
    
    window.plugin.dispatchAssist.getLabelLayer = function(guid) {
        var portal = window.plugin.dispatchAssist.portalDispatchInfo[guid];
        if(parseInt(portal.cluster) == 1){
            return window.plugin.dispatchAssist.cluster1LayerGroup;
        } else if(parseInt(portal.cluster) == 2){
            return window.plugin.dispatchAssist.cluster2LayerGroup;
        } else if(parseInt(portal.cluster) == 3){
            return window.plugin.dispatchAssist.cluster3LayerGroup;
        } else if(parseInt(portal.cluster) == 4){
            return window.plugin.dispatchAssist.cluster4LayerGroup;
        } else {
            return window.plugin.dispatchAssist.clusterXLayerGroup;
        }
    };
    
    window.plugin.dispatchAssist.removeLabel = function(guid) {
        var labelLayer = window.plugin.dispatchAssist.getLabelLayer(guid);
        var previousLayer = window.plugin.dispatchAssist.labelLayers[guid];
        if(previousLayer) {
            labelLayer.removeLayer(previousLayer);
            delete plugin.dispatchAssist.labelLayers[guid];
        }
    };
    
    window.plugin.dispatchAssist.addLabel = function(guid, portal, marker) {
        
        if(!(guid in window.plugin.dispatchAssist.portalDispatchInfo && window.plugin.dispatchAssist.portalDispatchInfo[guid].codename !== undefined)) {
            return;
        }
        
        var portalName = window.plugin.dispatchAssist.portalDispatchInfo[guid].codename;
        var color = (portalName.charAt(0) == '*') ? "#ff0000" : window.plugin.dispatchAssist.options.color;
        if (marker && marker.bindLabel) {
            marker.bindLabel('<span class="portalLabel" style="color:' + color + ';">' + portalName + '</span>', { noHide: true });
            marker.showLabel();
            marker.hasLabel = true;
        }
    };
    
    window.plugin.dispatchAssist.updatePortalLabels = function() {
        
        var portals = {};
        
        for(var guid in window.plugin.dispatchAssist.portalDispatchInfo) {
            var p = window.portals[guid];
            if((p && p._map) || window.plugin.dispatchAssist.options.showAll) {
                // only consider portals added to the map or if we want all drawn
                var portal = window.plugin.dispatchAssist.portalDispatchInfo[guid];
                portals[guid] = portal;
            }
        }
        
        // and add those we do
        for(var guid in portals) {
            var portal = window.plugin.dispatchAssist.portalDispatchInfo[guid];
            var marker = window.portals[guid]
            layer = window.plugin.dispatchAssist.getLabelLayer(guid);
            if (marker && !marker.hasLabel) {
                marker.addTo(layer);
                window.plugin.dispatchAssist.addLabel(guid, portal, marker);
            }
        }
    };
    
    window.plugin.dispatchAssist.delayedUpdatePortalLabels = function(wait) {
        // as calculating portal marker visibility can take some time when there's lots of portals shown, we'll do it on
        // a short timer. this way it doesn't get repeated so much
        
        if(window.plugin.dispatchAssist.timer === undefined) {
            window.plugin.dispatchAssist.timer = setTimeout(function() {
                window.plugin.dispatchAssist.timer = undefined;
                window.plugin.dispatchAssist.updatePortalLabels();
            }, wait * 1000);
            
        }
    };
    
    window.plugin.dispatchAssist.portalDetail = function(data) {
        var guid = data.guid;
        var name = data.portalData.title;
        var clusterNumber = '';
        var codename = '';
        if(guid in window.plugin.dispatchAssist.portalDispatchInfo) {
            clusterNumber = window.plugin.dispatchAssist.portalDispatchInfo[guid].cluster || '-cluster-';
            codename = window.plugin.dispatchAssist.portalDispatchInfo[guid].codename || '-id-';
        } else {
            clusterNumber = '-cluster-';
            codename = '-id-';
        }
        var $text1 = $('<div id="clusterNumber">' + clusterNumber + '</div>');
        $text1.click(function() {
            setClusterNumber($text1, guid, name);
        });
        $('.imgpreview').after($text1);
        var $text2 = $('<div id="codename">' + codename + '</div>');
        $text2.click(function() {
            setCodename($text2, guid, name);
        });
        $text1.after($text2);
    };
    
    window.plugin.dispatchAssist.removeCodename = function(guid) {
        if(guid in window.plugin.dispatchAssist.portalDispatchInfo) {
            delete window.plugin.dispatchAssist.portalDispatchInfo[guid].codename;
            window.plugin.dispatchAssist.savePortalDispatchInfo();
            window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        }
    };
    
    window.plugin.dispatchAssist.removeCluster = function(guid) {
        if(guid in window.plugin.dispatchAssist.portalDispatchInfo) {
            delete window.plugin.dispatchAssist.portalDispatchInfo[guid].cluster;
            window.plugin.dispatchAssist.savePortalDispatchInfo();
            window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        }
    };
    
    window.plugin.dispatchAssist.savePortalDispatchInfo = function() {
        localStorage['plugin-dispatch-assist-portals'] = JSON.stringify(window.plugin.dispatchAssist.portalDispatchInfo);
        localStorage['plugin-dispatch-assist-options'] = JSON.stringify(window.plugin.dispatchAssist.options);
    };
    
    window.plugin.dispatchAssist.loadFromLocalStorage = function() {
        window.plugin.dispatchAssist.portalDispatchInfo = JSON.parse(localStorage['plugin-dispatch-assist-portals'] || '{}');
        window.plugin.dispatchAssist.options = JSON.parse(localStorage['plugin-dispatch-assist-options'] || '{"showAll":false, "allowOverlap":true, "color":"#FFFFFF"}');
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.displayOpt = function() {
        var html = '<div class="dispatchStyles">';
        html += '<span class="label">Label Color</span>';
        html += '<input type="color" name="dispatchColor" id="dispatch_color"></input><br>';
        html += '<span class="label">Show All Portals</span>';
        html += '<select name="showAll" id="show_all" onchange="window.plugin.dispatchAssist.changeShowAll()">';
        html += '<option value="yes" ';
        html += (window.plugin.dispatchAssist.options.showAll) ? 'selected' : '';
        html += '>yes</option>';
        html += '<option value="no" ';
        html += (window.plugin.dispatchAssist.options.showAll) ? '' : 'selected';
        html += '>no</option>';
        html += '</select><br>';
        html += '</div>';
        html += '<div id="dsptchSetbox">';
        html += '<a onclick="window.plugin.dispatchAssist.optCopy();return false;">Copy Dispatch List</a>';
        html += '<a onclick="window.plugin.dispatchAssist.optPaste();return false;">Paste Dispatch List</a>';
        html += '<a onclick="window.plugin.dispatchAssist.optReset();return false;">Reset Dispatch List</a>';
        html += '</div>';
        dialog({
            html: html,
            dialogClass: 'ui-dialog-dsptchSet',
            title: 'Dispatch Assistant Options'
        });
        
        $('#dispatch_color').spectrum({
            flat: false,
            showInput: false,
            showButtons: false,
            showPalette: true,
            showSelectionPalette: false,
            palette: [
                ['#a24ac3', '#514ac3', '#4aa8c3', '#51c34a'],
                ['#c1c34a', '#c38a4a', '#c34a4a', '#c34a6f'],
                ['#000000', '#666666', '#bbbbbb', '#ffffff']
            ],
            change: function(color) {
                window.plugin.dispatchAssist.setLabelColor(color.toHexString());
            },
            color: window.plugin.dispatchAssist.options.color,
        });
    };
    
    window.plugin.dispatchAssist.setClusterToRemove = function(){
        var selectedOption = document.getElementById("remove_cluster");
        var selectedCluster = selectedOption.options[selectedOption.selectedIndex].value;
        if(confirm("Are you sure you want to remove all portals in cluster " + selectedCluster + "?")){
            window.plugin.dispatchAssist.removeFromCluster(selectedCluster);
        }
    }
    
    window.plugin.dispatchAssist.changeShowAll = function() {
        var selectedOption = document.getElementById("show_all");
        window.plugin.dispatchAssist.options.showAll = selectedOption.options[selectedOption.selectedIndex].value == "yes" ? true : false;
        window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.changeAllowOverlap = function() {
        var selectedOption = document.getElementById("allow_overlap");
        window.plugin.dispatchAssist.options.allowOverlap = selectedOption.options[selectedOption.selectedIndex].value == "yes" ? true : false;
        window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.setLabelColor = function(color) {
        window.plugin.dispatchAssist.options.color = color;
        window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.optReset = function() {
        $.each(window.plugin.dispatchAssist.portalDispatchInfo, function(guid, value) {
            window.plugin.dispatchAssist.removeCodename(guid);
            window.plugin.dispatchAssist.removeCluster(guid);
            delete window.plugin.dispatchAssist.portalDispatchInfo[guid];
            window.plugin.dispatchAssist.savePortalDispatchInfo();
        });
        window.plugin.dispatchAssist.portalDispatchInfo = {};
        window.plugin.dispatchAssist.savePortalDispatchInfo();
    };
    
    window.plugin.dispatchAssist.optCopy = function() {
        if(typeof android !== 'undefined' && android && android.shareString) {
            android.shareString(localStorage['plugin-dispatch-assist-portals']);
        } else {
            dialog({
                html: '<p><a onclick="$(\'.ui-dialog-dispatch-copy textarea\').select();">Select all</a> and press CTRL+C to copy it.</p><textarea readonly onclick="$(\'.ui-dialog-dispatch-copy textarea\').select();">' + localStorage['plugin-dispatch-assist-portals'] + '</textarea>',
                width: 600,
                dialogClass: 'ui-dialog-dispatch-copy',
                title: 'Codenames Export'
            });
        }
    };
    
    window.plugin.dispatchAssist.optPaste = function() {
        var promptAction = prompt('Press CTRL+V to paste it.', '');
        if(promptAction !== null && promptAction !== '') {
            try {
                var data = JSON.parse(promptAction);
                var goodData = {};
                $.each(data, function(guid, innerData) {
                    if(Object.keys(innerData).length > 0) {
                        if(innerData instanceof Object) {
                            goodData[guid] = innerData;
                        } else {
                            goodData[guid] = {
                                'codename': innerData
                            };
                        }
                    }
                });
                
                window.plugin.dispatchAssist.portalDispatchInfo = $.extend(window.plugin.dispatchAssist.portalDispatchInfo, goodData);
                window.plugin.dispatchAssist.savePortalDispatchInfo();
                window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
            } catch(e) {
                console.warn('DispatchAssist: failed to import data: ' + e);
                window.plugin.dispatchAssist.optAlert('<span style="color: #f88">Import failed</span>');
            }
        }
    };
    
    function removeCodename($text, guid) {
        if(guid in window.plugin.dispatchAssist.portalDispatchInfo) {
            window.plugin.dispatchAssist.removeCodename(guid);
            $text.text("--");
        }
    }
    
    function setClusterNumber($text, guid, name) {
        var currentCluster = (guid in window.plugin.dispatchAssist.portalDispatchInfo) ? window.plugin.dispatchAssist.portalDispatchInfo[guid]['cluster'] || '' : '';
        var newCluster = prompt('Enter cluster number for ' + name, currentCluster);
        
        var portal = window.portals[guid];
        var d = portal.options.data;
        var coord = portal.getLatLng();
        
        if(newCluster) {
            $text.text(newCluster);
            if(window.plugin.dispatchAssist.portalDispatchInfo[guid] !== undefined) {
                window.plugin.dispatchAssist.portalDispatchInfo[guid].cluster = newCluster;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].lat = coord.lat;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].lng = coord.lng;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].image = d.image;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].name = d.title;
            } else {
                window.plugin.dispatchAssist.portalDispatchInfo[guid] = {
                    'cluster': newCluster,
                    'lat': coord.lat,
                    'lng': coord.lng,
                    'image': d.image,
                    'name': d.title,
                };
            }
            window.plugin.dispatchAssist.savePortalDispatchInfo();
        }
    }
    
    function setCodename($text, guid, name) {
        var currentCodename = (guid in window.plugin.dispatchAssist.portalDispatchInfo) ? window.plugin.dispatchAssist.portalDispatchInfo[guid]['codename'] || '' : '';
        var newCodename = prompt('Enter codename for ' + name, currentCodename);
        
        var portal = window.portals[guid];
        var d = portal.options.data;
        var coord = portal.getLatLng();
        
        if(newCodename) {
            $text.text(newCodename);
            if(window.plugin.dispatchAssist.portalDispatchInfo[guid] !== undefined) {
                window.plugin.dispatchAssist.portalDispatchInfo[guid].codename = newCodename;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].lat = coord.lat;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].lng = coord.lng;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].image = d.image;
                window.plugin.dispatchAssist.portalDispatchInfo[guid].name = d.title;
            } else {
                window.plugin.dispatchAssist.portalDispatchInfo[guid] = {
                    'codename': newCodename,
                    'lat': coord.lat,
                    'lng': coord.lng,
                    'image': d.image,
                    'name': d.title,
                };
            }
            window.plugin.dispatchAssist.savePortalDispatchInfo();
            window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.1);
        }
    }
    
    
    window.plugin.dispatchAssist.boot = function() {
        window.plugin.dispatchAssist.setupCSS();
        
        window.plugin.dispatchAssist.loadFromLocalStorage();
        
        window.plugin.dispatchAssist.cluster1LayerGroup = new L.LayerGroup();
        window.plugin.dispatchAssist.cluster2LayerGroup = new L.LayerGroup();
        window.plugin.dispatchAssist.cluster3LayerGroup = new L.LayerGroup();
        window.plugin.dispatchAssist.cluster4LayerGroup = new L.LayerGroup();
        window.plugin.dispatchAssist.clusterXLayerGroup = new L.LayerGroup();
        window.addLayerGroup('Cluster 1 Portals', window.plugin.dispatchAssist.cluster1LayerGroup, true);
        window.addLayerGroup('Cluster 2 Portals', window.plugin.dispatchAssist.cluster2LayerGroup, true);
        window.addLayerGroup('Cluster 3 Portals', window.plugin.dispatchAssist.cluster3LayerGroup, true);
        window.addLayerGroup('Cluster 4 Portals', window.plugin.dispatchAssist.cluster4LayerGroup, true);
        window.addLayerGroup('Other Cluster Portals', window.plugin.dispatchAssist.clusterXLayerGroup, true);
		
        window.addHook('mapDataRefreshEnd', function() {
            window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.5);
        });
        window.map.on('overlayadd overlayremove', function() {
            setTimeout(function() {
                window.plugin.dispatchAssist.delayedUpdatePortalLabels(0.8);
            }, 1);
        });
        
        $('#toolbox').append(' <a onclick="window.plugin.dispatchAssist.displayPL()" title="Display a list of portals in the current view">Dispatch list</a>');
        $('#toolbox').append(' <a onclick="window.plugin.dispatchAssist.displayOpt()" title="Auto-assign cluster portals and Display List">Dispatch Opt</a>');
        $('#toolbox').append(' <a onclick="window.plugin.dispatchAssist.getClustersAndDisplayPL()" title="Auto-assign cluster portals and Display List">Auto-Cluster</a>');
        
        $('head').append('<style>' +
                         '#dispatchlist.mobile {background: transparent; border: 0 none !important; height: 100% !important; width: 100% !important; left: 0 !important; top: 0 !important; position: absolute; overflow: auto; }' +
                         '#dispatchlist table { margin-top:5px; border-collapse: collapse; empty-cells: show; width: 100%; clear: both; }' +
                         '#dispatchlist table td, #dispatchlist table th {border-bottom: 1px solid #0b314e; padding:3px; color:white; background-color:#1b415e}' +
                         '#dispatchlist table tr.res td { background-color: #005684; }' +
                         '#dispatchlist table tr.enl td { background-color: #017f01; }' +
                         '#dispatchlist table tr.neutral td { background-color: #000000; }' +
                         '#dispatchlist table th { text-align: center; }' +
                         '#dispatchlist table td { text-align: center; }' +
                         '#dispatchlist table.portals td { white-space: nowrap; }' +
                         '#dispatchlist table td.portalTitle { text-align: left;}' +
                         '#dispatchlist table th.sortable { cursor:pointer;}' +
                         '#dispatchlist table th.portalTitle { text-align: left;}' +
                         '#dispatchlist table .portalTitle { min-width: 120px !important; max-width: 240px !important; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; }' +
                         '#dispatchlist table .apGain { text-align: right !important; }' +
                         '#dispatchlist .sorted { color:#FFCE00; }' +
                         '#dispatchlist .filterAll { margin-top: 10px;}' +
                         '#dispatchlist .filterRes { margin-top: 10px; background-color: #005684  }' +
                         '#dispatchlist .filterEnl { margin-top: 10px; background-color: #017f01  }' +
                         '#dispatchlist .disclaimer { margin-top: 10px; font-size:10px; }' +
                         '#dsptchSetbox{text-align: center; }' +
                         '#dsptchStyles{text-align: center; }' +
                         '#dsptchSetbox a.disabled, #bkmrksSetbox a.disabled:hover { border-color: #666; color: #666; text-decoration: none; }' +
                         '#dsptchSetbox a { background: rgba(8,48,78,.9); border: 1px solid #ffce00; color: #ffce00; display: block; margin: 10px auto; padding: 3px 0; text-align: center; width: 80%; }' +
                         '</style>');
        
        $(document).on('click.dispatchAssist', '.portalClusterNumber', function(e) {
            var self = e.target,
                $self = $(self),
                guid = $self.data('guid');
            setClusterNumber($self, guid, $self.data('name'));
        })
        
        $(document).on('click.dispatchAssist', '.portalCodename', function(e) {
            var self = e.target,
                $self = $(self),
                guid = $self.data('guid');
            setCodename($self, guid, $self.data('name'));
        })
        
        $(document).on('click.dispatchAssist', '.portalCodenameDelete', function(e) {
            var self = e.target,
                $self = $(self),
                guid = $self.data('guid');
            removeCodename($self, guid);
        })
        
        window.addHook('portalDetailsUpdated', window.plugin.dispatchAssist.portalDetail);
        $('head').append('<style>' +
                         '.res #codename { border: 1px solid #0076b6; }' +
                         '.enl #codename { border: 1px solid #017f01; }' +
                         '#codename { width:44%; display: inline-block; padding: 3px; margin: 2px 7px 2px 3px; font-size: 12px; background-color: rgba(0, 0, 0, 0.7); text-align: center; overflow: hidden; }' +
                         '.res #clusterNumber { border: 1px solid #0076b6; }' +
                         '.enl #clusterNumber { border: 1px solid #017f01; }' +
                         '#clusterNumber { width:44%; display: inline-block; padding: 3px; margin: 2px 3px 2px 5px; font-size: 12px; background-color: rgba(0, 0, 0, 0.7); text-align: center; overflow: hidden; }' +
                         '</style>');
    }
    
    // PLUGIN END //////////////////////////////////////////////////////////    
    window.plugin.dispatchAssist.loadExternals = function() {
        
        // Spectrum Colorpicker v1.2.0
        // https://github.com/bgrins/spectrum
        // Author: Brian Grinstead
        // License: MIT
        
        (function(window, $, undefined) {
            var defaultOpts = {
                
                // Callbacks
                beforeShow: noop,
                move: noop,
                change: noop,
                show: noop,
                hide: noop,
                
                // Options
                color: false,
                flat: false,
                showInput: false,
                allowEmpty: false,
                showButtons: true,
                clickoutFiresChange: false,
                showInitial: false,
                showPalette: false,
                showPaletteOnly: false,
                showSelectionPalette: true,
                localStorageKey: false,
                appendTo: "body",
                maxSelectionSize: 7,
                cancelText: "cancel",
                chooseText: "choose",
                preferredFormat: false,
                className: "",
                showAlpha: false,
                theme: "sp-light",
                palette: ['fff', '000'],
                selectionPalette: [],
                disabled: false
            },
                spectrums = [],
                IE = !!/msie/i.exec(window.navigator.userAgent),
                rgbaSupport = (function() {
                    function contains(str, substr) {
                        return !!~('' + str).indexOf(substr);
                    }
                    
                    var elem = document.createElement('div');
                    var style = elem.style;
                    style.cssText = 'background-color:rgba(0,0,0,.5)';
                    return contains(style.backgroundColor, 'rgba') || contains(style.backgroundColor, 'hsla');
                })(),
                inputTypeColorSupport = (function() {
                    var colorInput = $("<input type='color' value='!' />")[0];
                    return colorInput.type === "color" && colorInput.value !== "!";
                })(),
                replaceInput = [
                    "<div class='sp-replacer'>",
                    "<div class='sp-preview'><div class='sp-preview-inner'></div></div>",
                    "<div class='sp-dd'>&#9660;</div>",
                    "</div>"
                ].join(''),
                markup = (function() {
                    
                    // IE does not support gradients with multiple stops, so we need to simulate
                    //  that for the rainbow slider with 8 divs that each have a single gradient
                    var gradientFix = "";
                    if(IE) {
                        for(var i = 1; i <= 6; i++) {
                            gradientFix += "<div class='sp-" + i + "'></div>";
                        }
                    }
                    
                    return [
                        "<div class='sp-container sp-hidden'>",
                        "<div class='sp-palette-container'>",
                        "<div class='sp-palette sp-thumb sp-cf'></div>",
                        "</div>",
                        "<div class='sp-picker-container'>",
                        "<div class='sp-top sp-cf'>",
                        "<div class='sp-fill'></div>",
                        "<div class='sp-top-inner'>",
                        "<div class='sp-color'>",
                        "<div class='sp-sat'>",
                        "<div class='sp-val'>",
                        "<div class='sp-dragger'></div>",
                        "</div>",
                        "</div>",
                        "</div>",
                        "<div class='sp-clear sp-clear-display' title='Clear Color Selection'>",
                        "</div>",
                        "<div class='sp-hue'>",
                        "<div class='sp-slider'></div>",
                        gradientFix,
                        "</div>",
                        "</div>",
                        "<div class='sp-alpha'><div class='sp-alpha-inner'><div class='sp-alpha-handle'></div></div></div>",
                        "</div>",
                        "<div class='sp-input-container sp-cf'>",
                        "<input class='sp-input' type='text' spellcheck='false'  />",
                        "</div>",
                        "<div class='sp-initial sp-thumb sp-cf'></div>",
                        "<div class='sp-button-container sp-cf'>",
                        "<a class='sp-cancel' href='#'></a>",
                        "<button class='sp-choose'></button>",
                        "</div>",
                        "</div>",
                        "</div>"
                    ].join("");
                })();
            
            function paletteTemplate(p, color, className) {
                var html = [];
                for(var i = 0; i < p.length; i++) {
                    var current = p[i];
                    if(current) {
                        var tiny = tinycolor(current);
                        var c = tiny.toHsl().l < 0.5 ? "sp-thumb-el sp-thumb-dark" : "sp-thumb-el sp-thumb-light";
                        c += (tinycolor.equals(color, current)) ? " sp-thumb-active" : "";
                        
                        var swatchStyle = rgbaSupport ? ("background-color:" + tiny.toRgbString()) : "filter:" + tiny.toFilter();
                        html.push('<span title="' + tiny.toRgbString() + '" data-color="' + tiny.toRgbString() + '" class="' + c + '"><span class="sp-thumb-inner" style="' + swatchStyle + ';" /></span>');
                    } else {
                        var cls = 'sp-clear-display';
                        html.push('<span title="No Color Selected" data-color="" style="background-color:transparent;" class="' + cls + '"></span>');
                    }
                }
                return "<div class='sp-cf " + className + "'>" + html.join('') + "</div>";
            }
            
            function hideAll() {
                for(var i = 0; i < spectrums.length; i++) {
                    if(spectrums[i]) {
                        spectrums[i].hide();
                    }
                }
            }
            
            function instanceOptions(o, callbackContext) {
                var opts = $.extend({}, defaultOpts, o);
                opts.callbacks = {
                    'move': bind(opts.move, callbackContext),
                    'change': bind(opts.change, callbackContext),
                    'show': bind(opts.show, callbackContext),
                    'hide': bind(opts.hide, callbackContext),
                    'beforeShow': bind(opts.beforeShow, callbackContext)
                };
                
                return opts;
            }
            
            function spectrum(element, o) {
                
                var opts = instanceOptions(o, element),
                    flat = opts.flat,
                    showSelectionPalette = opts.showSelectionPalette,
                    localStorageKey = opts.localStorageKey,
                    theme = opts.theme,
                    callbacks = opts.callbacks,
                    resize = throttle(reflow, 10),
                    visible = false,
                    dragWidth = 0,
                    dragHeight = 0,
                    dragHelperHeight = 0,
                    slideHeight = 0,
                    slideWidth = 0,
                    alphaWidth = 0,
                    alphaSlideHelperWidth = 0,
                    slideHelperHeight = 0,
                    currentHue = 0,
                    currentSaturation = 0,
                    currentValue = 0,
                    currentAlpha = 1,
                    palette = opts.palette.slice(0),
                    paletteArray = $.isArray(palette[0]) ? palette : [palette],
                    selectionPalette = opts.selectionPalette.slice(0),
                    maxSelectionSize = opts.maxSelectionSize,
                    draggingClass = "sp-dragging",
                    shiftMovementDirection = null;
                
                var doc = element.ownerDocument,
                    body = doc.body,
                    boundElement = $(element),
                    disabled = false,
                    container = $(markup, doc).addClass(theme),
                    dragger = container.find(".sp-color"),
                    dragHelper = container.find(".sp-dragger"),
                    slider = container.find(".sp-hue"),
                    slideHelper = container.find(".sp-slider"),
                    alphaSliderInner = container.find(".sp-alpha-inner"),
                    alphaSlider = container.find(".sp-alpha"),
                    alphaSlideHelper = container.find(".sp-alpha-handle"),
                    textInput = container.find(".sp-input"),
                    paletteContainer = container.find(".sp-palette"),
                    initialColorContainer = container.find(".sp-initial"),
                    cancelButton = container.find(".sp-cancel"),
                    clearButton = container.find(".sp-clear"),
                    chooseButton = container.find(".sp-choose"),
                    isInput = boundElement.is("input"),
                    isInputTypeColor = isInput && inputTypeColorSupport && boundElement.attr("type") === "color",
                    shouldReplace = isInput && !flat,
                    replacer = (shouldReplace) ? $(replaceInput).addClass(theme).addClass(opts.className) : $([]),
                    offsetElement = (shouldReplace) ? replacer : boundElement,
                    previewElement = replacer.find(".sp-preview-inner"),
                    initialColor = opts.color || (isInput && boundElement.val()),
                    colorOnShow = false,
                    preferredFormat = opts.preferredFormat,
                    currentPreferredFormat = preferredFormat,
                    clickoutFiresChange = !opts.showButtons || opts.clickoutFiresChange,
                    isEmpty = !initialColor,
                    allowEmpty = opts.allowEmpty && !isInputTypeColor;
                
                function applyOptions() {
                    
                    if(opts.showPaletteOnly) {
                        opts.showPalette = true;
                    }
                    
                    container.toggleClass("sp-flat", flat);
                    container.toggleClass("sp-input-disabled", !opts.showInput);
                    container.toggleClass("sp-alpha-enabled", opts.showAlpha);
                    container.toggleClass("sp-clear-enabled", allowEmpty);
                    container.toggleClass("sp-buttons-disabled", !opts.showButtons);
                    container.toggleClass("sp-palette-disabled", !opts.showPalette);
                    container.toggleClass("sp-palette-only", opts.showPaletteOnly);
                    container.toggleClass("sp-initial-disabled", !opts.showInitial);
                    container.addClass(opts.className);
                    
                    reflow();
                }
                
                function initialize() {
                    
                    if(IE) {
                        container.find("*:not(input)").attr("unselectable", "on");
                    }
                    
                    applyOptions();
                    
                    if(shouldReplace) {
                        boundElement.after(replacer).hide();
                    }
                    
                    if(!allowEmpty) {
                        clearButton.hide();
                    }
                    
                    if(flat) {
                        boundElement.after(container).hide();
                    } else {
                        
                        var appendTo = opts.appendTo === "parent" ? boundElement.parent() : $(opts.appendTo);
                        if(appendTo.length !== 1) {
                            appendTo = $("body");
                        }
                        
                        appendTo.append(container);
                    }
                    
                    if(localStorageKey && window.localStorage) {
                        
                        // Migrate old palettes over to new format.  May want to remove this eventually.
                        try {
                            var oldPalette = window.localStorage[localStorageKey].split(",#");
                            if(oldPalette.length > 1) {
                                delete window.localStorage[localStorageKey];
                                $.each(oldPalette, function(i, c) {
                                    addColorToSelectionPalette(c);
                                });
                            }
                        } catch(e) {}
                        
                        try {
                            selectionPalette = window.localStorage[localStorageKey].split(";");
                        } catch(e) {}
                    }
                    
                    offsetElement.bind("click.spectrum touchstart.spectrum", function(e) {
                        if(!disabled) {
                            toggle();
                        }
                        
                        e.stopPropagation();
                        
                        if(!$(e.target).is("input")) {
                            e.preventDefault();
                        }
                    });
                    
                    if(boundElement.is(":disabled") || (opts.disabled === true)) {
                        disable();
                    }
                    
                    // Prevent clicks from bubbling up to document.  This would cause it to be hidden.
                    container.click(stopPropagation);
                    
                    // Handle user typed input
                    textInput.change(setFromTextInput);
                    textInput.bind("paste", function() {
                        setTimeout(setFromTextInput, 1);
                    });
                    textInput.keydown(function(e) {
                        if(e.keyCode == 13) {
                            setFromTextInput();
                        }
                    });
                    
                    cancelButton.text(opts.cancelText);
                    cancelButton.bind("click.spectrum", function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        hide("cancel");
                    });
                    
                    
                    clearButton.bind("click.spectrum", function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        
                        isEmpty = true;
                        
                        move();
                        if(flat) {
                            //for the flat style, this is a change event
                            updateOriginalInput(true);
                        }
                    });
                    
                    
                    chooseButton.text(opts.chooseText);
                    chooseButton.bind("click.spectrum", function(e) {
                        e.stopPropagation();
                        e.preventDefault();
                        
                        if(isValid()) {
                            updateOriginalInput(true);
                            hide();
                        }
                    });
                    
                    draggable(alphaSlider, function(dragX, dragY, e) {
                        currentAlpha = (dragX / alphaWidth);
                        isEmpty = false;
                        if(e.shiftKey) {
                            currentAlpha = Math.round(currentAlpha * 10) / 10;
                        }
                        
                        move();
                    });
                    
                    draggable(slider, function(dragX, dragY) {
                        currentHue = parseFloat(dragY / slideHeight);
                        isEmpty = false;
                        move();
                    }, dragStart, dragStop);
                    
                    draggable(dragger, function(dragX, dragY, e) {
                        
                        // shift+drag should snap the movement to either the x or y axis.
                        if(!e.shiftKey) {
                            shiftMovementDirection = null;
                        } else if(!shiftMovementDirection) {
                            var oldDragX = currentSaturation * dragWidth;
                            var oldDragY = dragHeight - (currentValue * dragHeight);
                            var furtherFromX = Math.abs(dragX - oldDragX) > Math.abs(dragY - oldDragY);
                            
                            shiftMovementDirection = furtherFromX ? "x" : "y";
                        }
                            
                            var setSaturation = !shiftMovementDirection || shiftMovementDirection === "x";
                        var setValue = !shiftMovementDirection || shiftMovementDirection === "y";
                        
                        if(setSaturation) {
                            currentSaturation = parseFloat(dragX / dragWidth);
                        }
                        if(setValue) {
                            currentValue = parseFloat((dragHeight - dragY) / dragHeight);
                        }
                        
                        isEmpty = false;
                        
                        move();
                        
                    }, dragStart, dragStop);
                    
                    if(!!initialColor) {
                        set(initialColor);
                        
                        // In case color was black - update the preview UI and set the format
                        // since the set function will not run (default color is black).
                        updateUI();
                        currentPreferredFormat = preferredFormat || tinycolor(initialColor).format;
                        
                        addColorToSelectionPalette(initialColor);
                    } else {
                        updateUI();
                    }
                    
                    if(flat) {
                        show();
                    }
                    
                    function palletElementClick(e) {
                        if(e.data && e.data.ignore) {
                            set($(this).data("color"));
                            move();
                        } else {
                            set($(this).data("color"));
                            updateOriginalInput(true);
                            move();
                            hide();
                        }
                        
                        return false;
                    }
                    
                    var paletteEvent = IE ? "mousedown.spectrum" : "click.spectrum touchstart.spectrum";
                    paletteContainer.delegate(".sp-thumb-el", paletteEvent, palletElementClick);
                    initialColorContainer.delegate(".sp-thumb-el:nth-child(1)", paletteEvent, {
                        ignore: true
                    }, palletElementClick);
                }
                
                function addColorToSelectionPalette(color) {
                    if(showSelectionPalette) {
                        var colorRgb = tinycolor(color).toRgbString();
                        if($.inArray(colorRgb, selectionPalette) === -1) {
                            selectionPalette.push(colorRgb);
                            while(selectionPalette.length > maxSelectionSize) {
                                selectionPalette.shift();
                            }
                        }
                        
                        if(localStorageKey && window.localStorage) {
                            try {
                                window.localStorage[localStorageKey] = selectionPalette.join(";");
                            } catch(e) {}
                        }
                    }
                }
                
                function getUniqueSelectionPalette() {
                    var unique = [];
                    var p = selectionPalette;
                    var paletteLookup = {};
                    var rgb;
                    
                    if(opts.showPalette) {
                        
                        for(var i = 0; i < paletteArray.length; i++) {
                            for(var j = 0; j < paletteArray[i].length; j++) {
                                rgb = tinycolor(paletteArray[i][j]).toRgbString();
                                paletteLookup[rgb] = true;
                            }
                        }
                        
                        for(i = 0; i < p.length; i++) {
                            rgb = tinycolor(p[i]).toRgbString();
                            
                            if(!paletteLookup.hasOwnProperty(rgb)) {
                                unique.push(p[i]);
                                paletteLookup[rgb] = true;
                            }
                        }
                    }
                    
                    return unique.reverse().slice(0, opts.maxSelectionSize);
                }
                
                function drawPalette() {
                    
                    var currentColor = get();
                    
                    var html = $.map(paletteArray, function(palette, i) {
                        return paletteTemplate(palette, currentColor, "sp-palette-row sp-palette-row-" + i);
                    });
                    
                    if(selectionPalette) {
                        html.push(paletteTemplate(getUniqueSelectionPalette(), currentColor, "sp-palette-row sp-palette-row-selection"));
                    }
                    
                    paletteContainer.html(html.join(""));
                }
                
                function drawInitial() {
                    if(opts.showInitial) {
                        var initial = colorOnShow;
                        var current = get();
                        initialColorContainer.html(paletteTemplate([initial, current], current, "sp-palette-row-initial"));
                    }
                }
                
                function dragStart() {
                    if(dragHeight <= 0 || dragWidth <= 0 || slideHeight <= 0) {
                        reflow();
                    }
                    container.addClass(draggingClass);
                    shiftMovementDirection = null;
                }
                
                function dragStop() {
                    container.removeClass(draggingClass);
                }
                
                function setFromTextInput() {
                    
                    var value = textInput.val();
                    
                    if((value === null || value === "") && allowEmpty) {
                        set(null);
                    } else {
                        var tiny = tinycolor(value);
                        if(tiny.ok) {
                            set(tiny);
                        } else {
                            textInput.addClass("sp-validation-error");
                        }
                    }
                }
                
                function toggle() {
                    if(visible) {
                        hide();
                    } else {
                        show();
                    }
                }
                
                function show() {
                    var event = $.Event('beforeShow.spectrum');
                    
                    if(visible) {
                        reflow();
                        return;
                    }
                    
                    boundElement.trigger(event, [get()]);
                    
                    if(callbacks.beforeShow(get()) === false || event.isDefaultPrevented()) {
                        return;
                    }
                    
                    hideAll();
                    visible = true;
                    
                    $(doc).bind("click.spectrum", hide);
                    $(window).bind("resize.spectrum", resize);
                    replacer.addClass("sp-active");
                    container.removeClass("sp-hidden");
                    
                    if(opts.showPalette) {
                        drawPalette();
                    }
                    reflow();
                    updateUI();
                    
                    colorOnShow = get();
                    
                    drawInitial();
                    callbacks.show(colorOnShow);
                    boundElement.trigger('show.spectrum', [colorOnShow]);
                }
                
                function hide(e) {
                    
                    // Return on right click
                    if(e && e.type == "click" && e.button == 2) {
                        return;
                    }
                    
                    // Return if hiding is unnecessary
                    if(!visible || flat) {
                        return;
                    }
                    visible = false;
                    
                    $(doc).unbind("click.spectrum", hide);
                    $(window).unbind("resize.spectrum", resize);
                    
                    replacer.removeClass("sp-active");
                    container.addClass("sp-hidden");
                    
                    var colorHasChanged = !tinycolor.equals(get(), colorOnShow);
                    
                    if(colorHasChanged) {
                        if(clickoutFiresChange && e !== "cancel") {
                            updateOriginalInput(true);
                        } else {
                            revert();
                        }
                    }
                    
                    callbacks.hide(get());
                    boundElement.trigger('hide.spectrum', [get()]);
                }
                
                function revert() {
                    set(colorOnShow, true);
                }
                
                function set(color, ignoreFormatChange) {
                    if(tinycolor.equals(color, get())) {
                        return;
                    }
                    
                    var newColor;
                    if(!color && allowEmpty) {
                        isEmpty = true;
                    } else {
                        isEmpty = false;
                        newColor = tinycolor(color);
                        var newHsv = newColor.toHsv();
                        
                        currentHue = (newHsv.h % 360) / 360;
                        currentSaturation = newHsv.s;
                        currentValue = newHsv.v;
                        currentAlpha = newHsv.a;
                    }
                    updateUI();
                    
                    if(newColor && newColor.ok && !ignoreFormatChange) {
                        currentPreferredFormat = preferredFormat || newColor.format;
                    }
                }
                
                function get(opts) {
                    opts = opts || {};
                    
                    if(allowEmpty && isEmpty) {
                        return null;
                    }
                    
                    return tinycolor.fromRatio({
                        h: currentHue,
                        s: currentSaturation,
                        v: currentValue,
                        a: Math.round(currentAlpha * 100) / 100
                    }, {
                        format: opts.format || currentPreferredFormat
                    });
                }
                
                function isValid() {
                    return !textInput.hasClass("sp-validation-error");
                }
                
                function move() {
                    updateUI();
                    
                    callbacks.move(get());
                    boundElement.trigger('move.spectrum', [get()]);
                }
                
                function updateUI() {
                    
                    textInput.removeClass("sp-validation-error");
                    
                    updateHelperLocations();
                    
                    // Update dragger background color (gradients take care of saturation and value).
                    var flatColor = tinycolor.fromRatio({
                        h: currentHue,
                        s: 1,
                        v: 1
                    });
                    dragger.css("background-color", flatColor.toHexString());
                    
                    // Get a format that alpha will be included in (hex and names ignore alpha)
                    var format = currentPreferredFormat;
                    if(currentAlpha < 1) {
                        if(format === "hex" || format === "hex3" || format === "hex6" || format === "name") {
                            format = "rgb";
                        }
                    }
                    
                    var realColor = get({
                        format: format
                    }),
                        displayColor = '';
                    
                    //reset background info for preview element
                    previewElement.removeClass("sp-clear-display");
                    previewElement.css('background-color', 'transparent');
                    
                    if(!realColor && allowEmpty) {
                        // Update the replaced elements background with icon indicating no color selection
                        previewElement.addClass("sp-clear-display");
                    } else {
                        var realHex = realColor.toHexString(),
                            realRgb = realColor.toRgbString();
                        
                        // Update the replaced elements background color (with actual selected color)
                        if(rgbaSupport || realColor.alpha === 1) {
                            previewElement.css("background-color", realRgb);
                        } else {
                            previewElement.css("background-color", "transparent");
                            previewElement.css("filter", realColor.toFilter());
                        }
                        
                        if(opts.showAlpha) {
                            var rgb = realColor.toRgb();
                            rgb.a = 0;
                            var realAlpha = tinycolor(rgb).toRgbString();
                            var gradient = "linear-gradient(left, " + realAlpha + ", " + realHex + ")";
                            
                            if(IE) {
                                alphaSliderInner.css("filter", tinycolor(realAlpha).toFilter({
                                    gradientType: 1
                                }, realHex));
                            } else {
                                alphaSliderInner.css("background", "-webkit-" + gradient);
                                alphaSliderInner.css("background", "-moz-" + gradient);
                                alphaSliderInner.css("background", "-ms-" + gradient);
                                alphaSliderInner.css("background", gradient);
                            }
                        }
                        
                        displayColor = realColor.toString(format);
                    }
                    // Update the text entry input as it changes happen
                    if(opts.showInput) {
                        textInput.val(displayColor);
                    }
                    
                    if(opts.showPalette) {
                        drawPalette();
                    }
                    
                    drawInitial();
                }
                
                function updateHelperLocations() {
                    var s = currentSaturation;
                    var v = currentValue;
                    
                    if(allowEmpty && isEmpty) {
                        //if selected color is empty, hide the helpers
                        alphaSlideHelper.hide();
                        slideHelper.hide();
                        dragHelper.hide();
                    } else {
                        //make sure helpers are visible
                        alphaSlideHelper.show();
                        slideHelper.show();
                        dragHelper.show();
                        
                        // Where to show the little circle in that displays your current selected color
                        var dragX = s * dragWidth;
                        var dragY = dragHeight - (v * dragHeight);
                        dragX = Math.max(-dragHelperHeight,
                                         Math.min(dragWidth - dragHelperHeight, dragX - dragHelperHeight)
                                        );
                        dragY = Math.max(-dragHelperHeight,
                                         Math.min(dragHeight - dragHelperHeight, dragY - dragHelperHeight)
                                        );
                        dragHelper.css({
                            "top": dragY,
                            "left": dragX
                        });
                        
                        var alphaX = currentAlpha * alphaWidth;
                        alphaSlideHelper.css({
                            "left": alphaX - (alphaSlideHelperWidth / 2)
                        });
                        
                        // Where to show the bar that displays your current selected hue
                        var slideY = (currentHue) * slideHeight;
                        slideHelper.css({
                            "top": slideY - slideHelperHeight
                        });
                    }
                }
                
                function updateOriginalInput(fireCallback) {
                    var color = get(),
                        displayColor = '',
                        hasChanged = !tinycolor.equals(color, colorOnShow);
                    
                    if(color) {
                        displayColor = color.toString(currentPreferredFormat);
                        // Update the selection palette with the current color
                        addColorToSelectionPalette(color);
                    }
                    
                    if(isInput) {
                        boundElement.val(displayColor);
                    }
                    
                    colorOnShow = color;
                    
                    if(fireCallback && hasChanged) {
                        callbacks.change(color);
                        boundElement.trigger('change', [color]);
                    }
                }
                
                function reflow() {
                    dragWidth = dragger.width();
                    dragHeight = dragger.height();
                    dragHelperHeight = dragHelper.height();
                    slideWidth = slider.width();
                    slideHeight = slider.height();
                    slideHelperHeight = slideHelper.height();
                    alphaWidth = alphaSlider.width();
                    alphaSlideHelperWidth = alphaSlideHelper.width();
                    
                    if(!flat) {
                        container.css("position", "absolute");
                        container.offset(getOffset(container, offsetElement));
                    }
                    
                    updateHelperLocations();
                }
                
                function destroy() {
                    boundElement.show();
                    offsetElement.unbind("click.spectrum touchstart.spectrum");
                    container.remove();
                    replacer.remove();
                    spectrums[spect.id] = null;
                }
                
                function option(optionName, optionValue) {
                    if(optionName === undefined) {
                        return $.extend({}, opts);
                    }
                    if(optionValue === undefined) {
                        return opts[optionName];
                    }
                    
                    opts[optionName] = optionValue;
                    applyOptions();
                }
                
                function enable() {
                    disabled = false;
                    boundElement.attr("disabled", false);
                    offsetElement.removeClass("sp-disabled");
                }
                
                function disable() {
                    hide();
                    disabled = true;
                    boundElement.attr("disabled", true);
                    offsetElement.addClass("sp-disabled");
                }
                
                initialize();
                
                var spect = {
                    show: show,
                    hide: hide,
                    toggle: toggle,
                    reflow: reflow,
                    option: option,
                    enable: enable,
                    disable: disable,
                    set: function(c) {
                        set(c);
                        updateOriginalInput();
                    },
                    get: get,
                    destroy: destroy,
                    container: container
                };
                
                spect.id = spectrums.push(spect) - 1;
                
                return spect;
            }
            
            /**
         * checkOffset - get the offset below/above and left/right element depending on screen position
         * Thanks https://github.com/jquery/jquery-ui/blob/master/ui/jquery.ui.datepicker.js
         */
            function getOffset(picker, input) {
                var extraY = 0;
                var dpWidth = picker.outerWidth();
                var dpHeight = picker.outerHeight();
                var inputHeight = input.outerHeight();
                var doc = picker[0].ownerDocument;
                var docElem = doc.documentElement;
                var viewWidth = docElem.clientWidth + $(doc).scrollLeft();
                var viewHeight = docElem.clientHeight + $(doc).scrollTop();
                var offset = input.offset();
                offset.top += inputHeight;
                
                offset.left -=
                    Math.min(offset.left, (offset.left + dpWidth > viewWidth && viewWidth > dpWidth) ?
                             Math.abs(offset.left + dpWidth - viewWidth) : 0);
                
                offset.top -=
                    Math.min(offset.top, ((offset.top + dpHeight > viewHeight && viewHeight > dpHeight) ?
                                          Math.abs(dpHeight + inputHeight - extraY) : extraY));
                
                return offset;
            }
            
            /**
         * noop - do nothing
         */
            function noop() {
                
            }
            
            /**
         * stopPropagation - makes the code only doing this a little easier to read in line
         */
          function stopPropagation(e) {
              e.stopPropagation();
          }
          
          /**
         * Create a function bound to a given object
         * Thanks to underscore.js
         */
          function bind(func, obj) {
              var slice = Array.prototype.slice;
              var args = slice.call(arguments, 2);
              return function() {
                  return func.apply(obj, args.concat(slice.call(arguments)));
              };
          }
          
          /**
         * Lightweight drag helper.  Handles containment within the element, so that
         * when dragging, the x is within [0,element.width] and y is within [0,element.height]
         */
          function draggable(element, onmove, onstart, onstop) {
              onmove = onmove || function() {};
              onstart = onstart || function() {};
              onstop = onstop || function() {};
              var doc = element.ownerDocument || document;
              var dragging = false;
              var offset = {};
              var maxHeight = 0;
              var maxWidth = 0;
              var hasTouch = ('ontouchstart' in window);
              
              var duringDragEvents = {};
              duringDragEvents["selectstart"] = prevent;
              duringDragEvents["dragstart"] = prevent;
              duringDragEvents["touchmove mousemove"] = move;
              duringDragEvents["touchend mouseup"] = stop;
              
              function prevent(e) {
                  if(e.stopPropagation) {
                      e.stopPropagation();
                  }
                  if(e.preventDefault) {
                      e.preventDefault();
                  }
                  e.returnValue = false;
              }
              
              function move(e) {
                  if(dragging) {
                      // Mouseup happened outside of window
                      if(IE && document.documentMode < 9 && !e.button) {
                          return stop();
                      }
                      
                      var touches = e.originalEvent.touches;
                      var pageX = touches ? touches[0].pageX : e.pageX;
                      var pageY = touches ? touches[0].pageY : e.pageY;
                      
                      var dragX = Math.max(0, Math.min(pageX - offset.left, maxWidth));
                      var dragY = Math.max(0, Math.min(pageY - offset.top, maxHeight));
                      
                      if(hasTouch) {
                          // Stop scrolling in iOS
                          prevent(e);
                      }
                      
                      onmove.apply(element, [dragX, dragY, e]);
                  }
              }
              
              function start(e) {
                  var rightclick = (e.which) ? (e.which == 3) : (e.button == 2);
                  var touches = e.originalEvent.touches;
                  
                  if(!rightclick && !dragging) {
                      if(onstart.apply(element, arguments) !== false) {
                          dragging = true;
                          maxHeight = $(element).height();
                          maxWidth = $(element).width();
                          offset = $(element).offset();
                          
                          $(doc).bind(duringDragEvents);
                          $(doc.body).addClass("sp-dragging");
                          
                          if(!hasTouch) {
                              move(e);
                          }
                          
                          prevent(e);
                      }
                  }
              }
              
              function stop() {
                  if(dragging) {
                      $(doc).unbind(duringDragEvents);
                      $(doc.body).removeClass("sp-dragging");
                      onstop.apply(element, arguments);
                  }
                  dragging = false;
              }
              
              $(element).bind("touchstart mousedown", start);
          }
          
          function throttle(func, wait, debounce) {
              var timeout;
              return function() {
                  var context = this,
                      args = arguments;
                  var throttler = function() {
                      timeout = null;
                      func.apply(context, args);
                  };
                  if(debounce) clearTimeout(timeout);
                  if(debounce || !timeout) timeout = setTimeout(throttler, wait);
              };
          }
          
          
          function log() { /* jshint -W021 */
              if(window.console) {
                  if(Function.prototype.bind) log = Function.prototype.bind.call(console.log, console);
                  else log = function() {
                      Function.prototype.apply.call(console.log, console, arguments);
                  };
                  log.apply(this, arguments);
              }
          }
          
          /**
         * Define a jQuery plugin
         */
          var dataID = "spectrum.id";
          $.fn.spectrum = function(opts, extra) {
              
              if(typeof opts == "string") {
                  
                  var returnValue = this;
                  var args = Array.prototype.slice.call(arguments, 1);
                  
                  this.each(function() {
                      var spect = spectrums[$(this).data(dataID)];
                      if(spect) {
                          
                          var method = spect[opts];
                          if(!method) {
                              throw new Error("Spectrum: no such method: '" + opts + "'");
                          }
                          
                          if(opts == "get") {
                              returnValue = spect.get();
                          } else if(opts == "container") {
                              returnValue = spect.container;
                          } else if(opts == "option") {
                              returnValue = spect.option.apply(spect, args);
                          } else if(opts == "destroy") {
                              spect.destroy();
                              $(this).removeData(dataID);
                          } else {
                              method.apply(spect, args);
                          }
                      }
                  });
                  
                  return returnValue;
              }
              
              // Initializing a new instance of spectrum
              return this.spectrum("destroy").each(function() {
                  var spect = spectrum(this, opts);
                  $(this).data(dataID, spect.id);
              });
          };
          
          $.fn.spectrum.load = true;
          $.fn.spectrum.loadOpts = {};
          $.fn.spectrum.draggable = draggable;
          $.fn.spectrum.defaults = defaultOpts;
          
          $.spectrum = {};
          $.spectrum.localization = {};
          $.spectrum.palettes = {};
          
          $.fn.spectrum.processNativeColorInputs = function() {
              if(!inputTypeColorSupport) {
                  $("input[type=color]").spectrum({
                      preferredFormat: "hex6"
                  });
              }
          };
          
          // TinyColor v0.9.16
          // https://github.com/bgrins/TinyColor
          // 2013-08-10, Brian Grinstead, MIT License
          
          (function() {
              
              var trimLeft = /^[\s,#]+/,
                  trimRight = /\s+$/,
                  tinyCounter = 0,
                  math = Math,
                  mathRound = math.round,
                  mathMin = math.min,
                  mathMax = math.max,
                  mathRandom = math.random;
              
              function tinycolor(color, opts) {
                  
                  color = (color) ? color : '';
                  opts = opts || {};
                  
                  // If input is already a tinycolor, return itself
                  if(typeof color == "object" && color.hasOwnProperty("_tc_id")) {
                      return color;
                  }
                  
                  var rgb = inputToRGB(color);
                  var r = rgb.r,
                      g = rgb.g,
                      b = rgb.b,
                      a = rgb.a,
                      roundA = mathRound(100 * a) / 100,
                      format = opts.format || rgb.format;
                  
                  // Don't let the range of [0,255] come back in [0,1].
                  // Potentially lose a little bit of precision here, but will fix issues where
                  // .5 gets interpreted as half of the total, instead of half of 1
                  // If it was supposed to be 128, this was already taken care of by `inputToRgb`
                  if(r < 1) {
                      r = mathRound(r);
                  }
                  if(g < 1) {
                      g = mathRound(g);
                  }
                  if(b < 1) {
                      b = mathRound(b);
                  }
                  
                  return {
                      ok: rgb.ok,
                      format: format,
                      _tc_id: tinyCounter++,
                      alpha: a,
                      getAlpha: function() {
                          return a;
                      },
                      setAlpha: function(value) {
                          a = boundAlpha(value);
                          roundA = mathRound(100 * a) / 100;
                      },
                      toHsv: function() {
                          var hsv = rgbToHsv(r, g, b);
                          return {
                              h: hsv.h * 360,
                              s: hsv.s,
                              v: hsv.v,
                              a: a
                          };
                      },
                      toHsvString: function() {
                          var hsv = rgbToHsv(r, g, b);
                          var h = mathRound(hsv.h * 360),
                              s = mathRound(hsv.s * 100),
                              v = mathRound(hsv.v * 100);
                          return(a == 1) ?
                              "hsv(" + h + ", " + s + "%, " + v + "%)" :
                          "hsva(" + h + ", " + s + "%, " + v + "%, " + roundA + ")";
                      },
                      toHsl: function() {
                          var hsl = rgbToHsl(r, g, b);
                          return {
                              h: hsl.h * 360,
                              s: hsl.s,
                              l: hsl.l,
                              a: a
                          };
                      },
                      toHslString: function() {
                          var hsl = rgbToHsl(r, g, b);
                          var h = mathRound(hsl.h * 360),
                              s = mathRound(hsl.s * 100),
                              l = mathRound(hsl.l * 100);
                          return(a == 1) ?
                              "hsl(" + h + ", " + s + "%, " + l + "%)" :
                          "hsla(" + h + ", " + s + "%, " + l + "%, " + roundA + ")";
                      },
                      toHex: function(allow3Char) {
                          return rgbToHex(r, g, b, allow3Char);
                      },
                      toHexString: function(allow3Char) {
                          return '#' + rgbToHex(r, g, b, allow3Char);
                      },
                      toRgb: function() {
                          return {
                              r: mathRound(r),
                              g: mathRound(g),
                              b: mathRound(b),
                              a: a
                          };
                      },
                      toRgbString: function() {
                          return(a == 1) ?
                              "rgb(" + mathRound(r) + ", " + mathRound(g) + ", " + mathRound(b) + ")" :
                          "rgba(" + mathRound(r) + ", " + mathRound(g) + ", " + mathRound(b) + ", " + roundA + ")";
                      },
                      toPercentageRgb: function() {
                          return {
                              r: mathRound(bound01(r, 255) * 100) + "%",
                              g: mathRound(bound01(g, 255) * 100) + "%",
                              b: mathRound(bound01(b, 255) * 100) + "%",
                              a: a
                          };
                      },
                      toPercentageRgbString: function() {
                          return(a == 1) ?
                              "rgb(" + mathRound(bound01(r, 255) * 100) + "%, " + mathRound(bound01(g, 255) * 100) + "%, " + mathRound(bound01(b, 255) * 100) + "%)" :
                          "rgba(" + mathRound(bound01(r, 255) * 100) + "%, " + mathRound(bound01(g, 255) * 100) + "%, " + mathRound(bound01(b, 255) * 100) + "%, " + roundA + ")";
                      },
                      toName: function() {
                          if(a === 0) {
                              return "transparent";
                          }
                          
                          return hexNames[rgbToHex(r, g, b, true)] || false;
                      },
                      toFilter: function(secondColor) {
                          var hex = rgbToHex(r, g, b);
                          var secondHex = hex;
                          var alphaHex = Math.round(parseFloat(a) * 255).toString(16);
                          var secondAlphaHex = alphaHex;
                          var gradientType = opts && opts.gradientType ? "GradientType = 1, " : "";
                          
                          if(secondColor) {
                              var s = tinycolor(secondColor);
                              secondHex = s.toHex();
                              secondAlphaHex = Math.round(parseFloat(s.alpha) * 255).toString(16);
                          }
                          
                          return "progid:DXImageTransform.Microsoft.gradient(" + gradientType + "startColorstr=#" + pad2(alphaHex) + hex + ",endColorstr=#" + pad2(secondAlphaHex) + secondHex + ")";
                      },
                      toString: function(format) {
                          var formatSet = !!format;
                          format = format || this.format;
                          
                          var formattedString = false;
                          var hasAlphaAndFormatNotSet = !formatSet && a < 1 && a > 0;
                          var formatWithAlpha = hasAlphaAndFormatNotSet && (format === "hex" || format === "hex6" || format === "hex3" || format === "name");
                          
                          if(format === "rgb") {
                              formattedString = this.toRgbString();
                          }
                          if(format === "prgb") {
                              formattedString = this.toPercentageRgbString();
                          }
                          if(format === "hex" || format === "hex6") {
                              formattedString = this.toHexString();
                          }
                          if(format === "hex3") {
                              formattedString = this.toHexString(true);
                          }
                          if(format === "name") {
                              formattedString = this.toName();
                          }
                          if(format === "hsl") {
                              formattedString = this.toHslString();
                          }
                          if(format === "hsv") {
                              formattedString = this.toHsvString();
                          }
                          
                          if(formatWithAlpha) {
                              return this.toRgbString();
                          }
                          
                          return formattedString || this.toHexString();
                      }
                  };
              }
              
              // If input is an object, force 1 into "1.0" to handle ratios properly
              // String input requires "1.0" as input, so 1 will be treated as 1
              tinycolor.fromRatio = function(color, opts) {
                  if(typeof color == "object") {
                      var newColor = {};
                      for(var i in color) {
                          if(color.hasOwnProperty(i)) {
                              if(i === "a") {
                                  newColor[i] = color[i];
                              } else {
                                  newColor[i] = convertToPercentage(color[i]);
                              }
                          }
                      }
                      color = newColor;
                  }
                  
                  return tinycolor(color, opts);
              };
              
              // Given a string or object, convert that input to RGB
              // Possible string inputs:
              //
              //     "red"
              //     "#f00" or "f00"
              //     "#ff0000" or "ff0000"
              //     "rgb 255 0 0" or "rgb (255, 0, 0)"
              //     "rgb 1.0 0 0" or "rgb (1, 0, 0)"
              //     "rgba (255, 0, 0, 1)" or "rgba 255, 0, 0, 1"
              //     "rgba (1.0, 0, 0, 1)" or "rgba 1.0, 0, 0, 1"
              //     "hsl(0, 100%, 50%)" or "hsl 0 100% 50%"
              //     "hsla(0, 100%, 50%, 1)" or "hsla 0 100% 50%, 1"
              //     "hsv(0, 100%, 100%)" or "hsv 0 100% 100%"
              //
              function inputToRGB(color) {
                  
                  var rgb = {
                      r: 0,
                      g: 0,
                      b: 0
                  };
                  var a = 1;
                  var ok = false;
                  var format = false;
                  
                  if(typeof color == "string") {
                      color = stringInputToObject(color);
                  }
                  
                  if(typeof color == "object") {
                      if(color.hasOwnProperty("r") && color.hasOwnProperty("g") && color.hasOwnProperty("b")) {
                          rgb = rgbToRgb(color.r, color.g, color.b);
                          ok = true;
                          format = String(color.r).substr(-1) === "%" ? "prgb" : "rgb";
                      } else if(color.hasOwnProperty("h") && color.hasOwnProperty("s") && color.hasOwnProperty("v")) {
                          color.s = convertToPercentage(color.s);
                          color.v = convertToPercentage(color.v);
                          rgb = hsvToRgb(color.h, color.s, color.v);
                          ok = true;
                          format = "hsv";
                      } else if(color.hasOwnProperty("h") && color.hasOwnProperty("s") && color.hasOwnProperty("l")) {
                          color.s = convertToPercentage(color.s);
                          color.l = convertToPercentage(color.l);
                          rgb = hslToRgb(color.h, color.s, color.l);
                          ok = true;
                          format = "hsl";
                      }
                          
                          if(color.hasOwnProperty("a")) {
                              a = color.a;
                          }
                  }
                  
                  a = boundAlpha(a);
                  
                  return {
                      ok: ok,
                      format: color.format || format,
                      r: mathMin(255, mathMax(rgb.r, 0)),
                      g: mathMin(255, mathMax(rgb.g, 0)),
                      b: mathMin(255, mathMax(rgb.b, 0)),
                      a: a
                  };
              }
              
              
              // Conversion Functions
              // --------------------
              
              // `rgbToHsl`, `rgbToHsv`, `hslToRgb`, `hsvToRgb` modified from:
              // <http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript>
              
              // `rgbToRgb`
              // Handle bounds / percentage checking to conform to CSS color spec
              // <http://www.w3.org/TR/css3-color/>
              // *Assumes:* r, g, b in [0, 255] or [0, 1]
              // *Returns:* { r, g, b } in [0, 255]
              function rgbToRgb(r, g, b) {
                  return {
                      r: bound01(r, 255) * 255,
                      g: bound01(g, 255) * 255,
                      b: bound01(b, 255) * 255
                  };
              }
              
              // `rgbToHsl`
              // Converts an RGB color value to HSL.
              // *Assumes:* r, g, and b are contained in [0, 255] or [0, 1]
              // *Returns:* { h, s, l } in [0,1]
              function rgbToHsl(r, g, b) {
                  
                  r = bound01(r, 255);
                  g = bound01(g, 255);
                  b = bound01(b, 255);
                  
                  var max = mathMax(r, g, b),
                      min = mathMin(r, g, b);
                  var h, s, l = (max + min) / 2;
                  
                  if(max == min) {
                      h = s = 0; // achromatic
                  } else {
                      var d = max - min;
                      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
                      switch(max) {
                          case r:
                              h = (g - b) / d + (g < b ? 6 : 0);
                              break;
                          case g:
                              h = (b - r) / d + 2;
                              break;
                          case b:
                              h = (r - g) / d + 4;
                              break;
                      }
                      
                      h /= 6;
                  }
                  
                  return {
                      h: h,
                      s: s,
                      l: l
                  };
              }
              
              // `hslToRgb`
              // Converts an HSL color value to RGB.
              // *Assumes:* h is contained in [0, 1] or [0, 360] and s and l are contained [0, 1] or [0, 100]
              // *Returns:* { r, g, b } in the set [0, 255]
              function hslToRgb(h, s, l) {
                  var r, g, b;
                  
                  h = bound01(h, 360);
                  s = bound01(s, 100);
                  l = bound01(l, 100);
                  
                  function hue2rgb(p, q, t) {
                      if(t < 0) t += 1;
                      if(t > 1) t -= 1;
                      if(t < 1 / 6) return p + (q - p) * 6 * t;
                      if(t < 1 / 2) return q;
                      if(t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
                      return p;
                  }
                  
                  if(s === 0) {
                      r = g = b = l; // achromatic
                  } else {
                      var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                      var p = 2 * l - q;
                      r = hue2rgb(p, q, h + 1 / 3);
                      g = hue2rgb(p, q, h);
                      b = hue2rgb(p, q, h - 1 / 3);
                  }
                  
                  return {
                      r: r * 255,
                      g: g * 255,
                      b: b * 255
                  };
              }
              
              // `rgbToHsv`
              // Converts an RGB color value to HSV
              // *Assumes:* r, g, and b are contained in the set [0, 255] or [0, 1]
              // *Returns:* { h, s, v } in [0,1]
              function rgbToHsv(r, g, b) {
                  
                  r = bound01(r, 255);
                  g = bound01(g, 255);
                  b = bound01(b, 255);
                  
                  var max = mathMax(r, g, b),
                      min = mathMin(r, g, b);
                  var h, s, v = max;
                  
                  var d = max - min;
                  s = max === 0 ? 0 : d / max;
                  
                  if(max == min) {
                      h = 0; // achromatic
                  } else {
                      switch(max) {
                          case r:
                              h = (g - b) / d + (g < b ? 6 : 0);
                              break;
                          case g:
                              h = (b - r) / d + 2;
                              break;
                          case b:
                              h = (r - g) / d + 4;
                              break;
                      }
                      h /= 6;
                  }
                  return {
                      h: h,
                      s: s,
                      v: v
                  };
              }
              
              // `hsvToRgb`
              // Converts an HSV color value to RGB.
              // *Assumes:* h is contained in [0, 1] or [0, 360] and s and v are contained in [0, 1] or [0, 100]
              // *Returns:* { r, g, b } in the set [0, 255]
              function hsvToRgb(h, s, v) {
                  
                  h = bound01(h, 360) * 6;
                  s = bound01(s, 100);
                  v = bound01(v, 100);
                  
                  var i = math.floor(h),
                      f = h - i,
                      p = v * (1 - s),
                      q = v * (1 - f * s),
                      t = v * (1 - (1 - f) * s),
                      mod = i % 6,
                      r = [v, q, p, p, t, v][mod],
                      g = [t, v, v, q, p, p][mod],
                      b = [p, p, t, v, v, q][mod];
                  
                  return {
                      r: r * 255,
                      g: g * 255,
                      b: b * 255
                  };
              }
              
              // `rgbToHex`
              // Converts an RGB color to hex
              // Assumes r, g, and b are contained in the set [0, 255]
              // Returns a 3 or 6 character hex
              function rgbToHex(r, g, b, allow3Char) {
                  
                  var hex = [
                      pad2(mathRound(r).toString(16)),
                      pad2(mathRound(g).toString(16)),
                      pad2(mathRound(b).toString(16))
                  ];
                  
                  // Return a 3 character hex if possible
                  if(allow3Char && hex[0].charAt(0) == hex[0].charAt(1) && hex[1].charAt(0) == hex[1].charAt(1) && hex[2].charAt(0) == hex[2].charAt(1)) {
                      return hex[0].charAt(0) + hex[1].charAt(0) + hex[2].charAt(0);
                  }
                  
                  return hex.join("");
              }
              
              // `equals`
              // Can be called with any tinycolor input
              tinycolor.equals = function(color1, color2) {
                  if(!color1 || !color2) {
                      return false;
                  }
                  return tinycolor(color1).toRgbString() == tinycolor(color2).toRgbString();
              };
              tinycolor.random = function() {
                  return tinycolor.fromRatio({
                      r: mathRandom(),
                      g: mathRandom(),
                      b: mathRandom()
                  });
              };
              
              
              // Modification Functions
              // ----------------------
              // Thanks to less.js for some of the basics here
              // <https://github.com/cloudhead/less.js/blob/master/lib/less/functions.js>
              
              tinycolor.desaturate = function(color, amount) {
                  amount = (amount === 0) ? 0 : (amount || 10);
                  var hsl = tinycolor(color).toHsl();
                  hsl.s -= amount / 100;
                  hsl.s = clamp01(hsl.s);
                  return tinycolor(hsl);
              };
              tinycolor.saturate = function(color, amount) {
                  amount = (amount === 0) ? 0 : (amount || 10);
                  var hsl = tinycolor(color).toHsl();
                  hsl.s += amount / 100;
                  hsl.s = clamp01(hsl.s);
                  return tinycolor(hsl);
              };
              tinycolor.greyscale = function(color) {
                  return tinycolor.desaturate(color, 100);
              };
              tinycolor.lighten = function(color, amount) {
                  amount = (amount === 0) ? 0 : (amount || 10);
                  var hsl = tinycolor(color).toHsl();
                  hsl.l += amount / 100;
                  hsl.l = clamp01(hsl.l);
                  return tinycolor(hsl);
              };
              tinycolor.darken = function(color, amount) {
                  amount = (amount === 0) ? 0 : (amount || 10);
                  var hsl = tinycolor(color).toHsl();
                  hsl.l -= amount / 100;
                  hsl.l = clamp01(hsl.l);
                  return tinycolor(hsl);
              };
              tinycolor.complement = function(color) {
                  var hsl = tinycolor(color).toHsl();
                  hsl.h = (hsl.h + 180) % 360;
                  return tinycolor(hsl);
              };
              
              
              // Combination Functions
              // ---------------------
              // Thanks to jQuery xColor for some of the ideas behind these
              // <https://github.com/infusion/jQuery-xcolor/blob/master/jquery.xcolor.js>
              
              tinycolor.triad = function(color) {
                  var hsl = tinycolor(color).toHsl();
                  var h = hsl.h;
                  return [
                      tinycolor(color),
                      tinycolor({
                          h: (h + 120) % 360,
                          s: hsl.s,
                          l: hsl.l
                      }),
                      tinycolor({
                          h: (h + 240) % 360,
                          s: hsl.s,
                          l: hsl.l
                      })
                  ];
              };
              tinycolor.tetrad = function(color) {
                  var hsl = tinycolor(color).toHsl();
                  var h = hsl.h;
                  return [
                      tinycolor(color),
                      tinycolor({
                          h: (h + 90) % 360,
                          s: hsl.s,
                          l: hsl.l
                      }),
                      tinycolor({
                          h: (h + 180) % 360,
                          s: hsl.s,
                          l: hsl.l
                      }),
                      tinycolor({
                          h: (h + 270) % 360,
                          s: hsl.s,
                          l: hsl.l
                      })
                  ];
              };
              tinycolor.splitcomplement = function(color) {
                  var hsl = tinycolor(color).toHsl();
                  var h = hsl.h;
                  return [
                      tinycolor(color),
                      tinycolor({
                          h: (h + 72) % 360,
                          s: hsl.s,
                          l: hsl.l
                      }),
                      tinycolor({
                          h: (h + 216) % 360,
                          s: hsl.s,
                          l: hsl.l
                      })
                  ];
              };
              tinycolor.analogous = function(color, results, slices) {
                  results = results || 6;
                  slices = slices || 30;
                  
                  var hsl = tinycolor(color).toHsl();
                  var part = 360 / slices;
                  var ret = [tinycolor(color)];
                  
                  for(hsl.h = ((hsl.h - (part * results >> 1)) + 720) % 360; --results;) {
                      hsl.h = (hsl.h + part) % 360;
                      ret.push(tinycolor(hsl));
                  }
                  return ret;
              };
              tinycolor.monochromatic = function(color, results) {
                  results = results || 6;
                  var hsv = tinycolor(color).toHsv();
                  var h = hsv.h,
                      s = hsv.s,
                      v = hsv.v;
                  var ret = [];
                  var modification = 1 / results;
                  
                  while(results--) {
                      ret.push(tinycolor({
                          h: h,
                          s: s,
                          v: v
                      }));
                      v = (v + modification) % 1;
                  }
                  
                  return ret;
              };
              
              
              // Readability Functions
              // ---------------------
              // <http://www.w3.org/TR/AERT#color-contrast>
              
              // `readability`
              // Analyze the 2 colors and returns an object with the following properties:
              //    `brightness`: difference in brightness between the two colors
              //    `color`: difference in color/hue between the two colors
              tinycolor.readability = function(color1, color2) {
                  var a = tinycolor(color1).toRgb();
                  var b = tinycolor(color2).toRgb();
                  var brightnessA = (a.r * 299 + a.g * 587 + a.b * 114) / 1000;
                  var brightnessB = (b.r * 299 + b.g * 587 + b.b * 114) / 1000;
                  var colorDiff = (
                      Math.max(a.r, b.r) - Math.min(a.r, b.r) +
                      Math.max(a.g, b.g) - Math.min(a.g, b.g) +
                      Math.max(a.b, b.b) - Math.min(a.b, b.b)
                  );
                  
                  return {
                      brightness: Math.abs(brightnessA - brightnessB),
                      color: colorDiff
                  };
              };
              
              // `readable`
              // http://www.w3.org/TR/AERT#color-contrast
              // Ensure that foreground and background color combinations provide sufficient contrast.
              // *Example*
              //    tinycolor.readable("#000", "#111") => false
              tinycolor.readable = function(color1, color2) {
                  var readability = tinycolor.readability(color1, color2);
                  return readability.brightness > 125 && readability.color > 500;
              };
              
              // `mostReadable`
              // Given a base color and a list of possible foreground or background
              // colors for that base, returns the most readable color.
              // *Example*
              //    tinycolor.mostReadable("#123", ["#fff", "#000"]) => "#000"
              tinycolor.mostReadable = function(baseColor, colorList) {
                  var bestColor = null;
                  var bestScore = 0;
                  var bestIsReadable = false;
                  for(var i = 0; i < colorList.length; i++) {
                      
                      // We normalize both around the "acceptable" breaking point,
                      // but rank brightness constrast higher than hue.
                      
                      var readability = tinycolor.readability(baseColor, colorList[i]);
                      var readable = readability.brightness > 125 && readability.color > 500;
                      var score = 3 * (readability.brightness / 125) + (readability.color / 500);
                      
                      if((readable && !bestIsReadable) ||
                         (readable && bestIsReadable && score > bestScore) ||
                         ((!readable) && (!bestIsReadable) && score > bestScore)) {
                          bestIsReadable = readable;
                          bestScore = score;
                          bestColor = tinycolor(colorList[i]);
                      }
                  }
                  return bestColor;
              };
              
              
              // Big List of Colors
              // ------------------
              // <http://www.w3.org/TR/css3-color/#svg-color>
              var names = tinycolor.names = {
                  aliceblue: "f0f8ff",
                  antiquewhite: "faebd7",
                  aqua: "0ff",
                  aquamarine: "7fffd4",
                  azure: "f0ffff",
                  beige: "f5f5dc",
                  bisque: "ffe4c4",
                  black: "000",
                  blanchedalmond: "ffebcd",
                  blue: "00f",
                  blueviolet: "8a2be2",
                  brown: "a52a2a",
                  burlywood: "deb887",
                  burntsienna: "ea7e5d",
                  cadetblue: "5f9ea0",
                  chartreuse: "7fff00",
                  chocolate: "d2691e",
                  coral: "ff7f50",
                  cornflowerblue: "6495ed",
                  cornsilk: "fff8dc",
                  crimson: "dc143c",
                  cyan: "0ff",
                  darkblue: "00008b",
                  darkcyan: "008b8b",
                  darkgoldenrod: "b8860b",
                  darkgray: "a9a9a9",
                  darkgreen: "006400",
                  darkgrey: "a9a9a9",
                  darkkhaki: "bdb76b",
                  darkmagenta: "8b008b",
                  darkolivegreen: "556b2f",
                  darkorange: "ff8c00",
                  darkorchid: "9932cc",
                  darkred: "8b0000",
                  darksalmon: "e9967a",
                  darkseagreen: "8fbc8f",
                  darkslateblue: "483d8b",
                  darkslategray: "2f4f4f",
                  darkslategrey: "2f4f4f",
                  darkturquoise: "00ced1",
                  darkviolet: "9400d3",
                  deeppink: "ff1493",
                  deepskyblue: "00bfff",
                  dimgray: "696969",
                  dimgrey: "696969",
                  dodgerblue: "1e90ff",
                  firebrick: "b22222",
                  floralwhite: "fffaf0",
                  forestgreen: "228b22",
                  fuchsia: "f0f",
                  gainsboro: "dcdcdc",
                  ghostwhite: "f8f8ff",
                  gold: "ffd700",
                  goldenrod: "daa520",
                  gray: "808080",
                  green: "008000",
                  greenyellow: "adff2f",
                  grey: "808080",
                  honeydew: "f0fff0",
                  hotpink: "ff69b4",
                  indianred: "cd5c5c",
                  indigo: "4b0082",
                  ivory: "fffff0",
                  khaki: "f0e68c",
                  lavender: "e6e6fa",
                  lavenderblush: "fff0f5",
                  lawngreen: "7cfc00",
                  lemonchiffon: "fffacd",
                  lightblue: "add8e6",
                  lightcoral: "f08080",
                  lightcyan: "e0ffff",
                  lightgoldenrodyellow: "fafad2",
                  lightgray: "d3d3d3",
                  lightgreen: "90ee90",
                  lightgrey: "d3d3d3",
                  lightpink: "ffb6c1",
                  lightsalmon: "ffa07a",
                  lightseagreen: "20b2aa",
                  lightskyblue: "87cefa",
                  lightslategray: "789",
                  lightslategrey: "789",
                  lightsteelblue: "b0c4de",
                  lightyellow: "ffffe0",
                  lime: "0f0",
                  limegreen: "32cd32",
                  linen: "faf0e6",
                  magenta: "f0f",
                  maroon: "800000",
                  mediumaquamarine: "66cdaa",
                  mediumblue: "0000cd",
                  mediumorchid: "ba55d3",
                  mediumpurple: "9370db",
                  mediumseagreen: "3cb371",
                  mediumslateblue: "7b68ee",
                  mediumspringgreen: "00fa9a",
                  mediumturquoise: "48d1cc",
                  mediumvioletred: "c71585",
                  midnightblue: "191970",
                  mintcream: "f5fffa",
                  mistyrose: "ffe4e1",
                  moccasin: "ffe4b5",
                  navajowhite: "ffdead",
                  navy: "000080",
                  oldlace: "fdf5e6",
                  olive: "808000",
                  olivedrab: "6b8e23",
                  orange: "ffa500",
                  orangered: "ff4500",
                  orchid: "da70d6",
                  palegoldenrod: "eee8aa",
                  palegreen: "98fb98",
                  paleturquoise: "afeeee",
                  palevioletred: "db7093",
                  papayawhip: "ffefd5",
                  peachpuff: "ffdab9",
                  peru: "cd853f",
                  pink: "ffc0cb",
                  plum: "dda0dd",
                  powderblue: "b0e0e6",
                  purple: "800080",
                  red: "f00",
                  rosybrown: "bc8f8f",
                  royalblue: "4169e1",
                  saddlebrown: "8b4513",
                  salmon: "fa8072",
                  sandybrown: "f4a460",
                  seagreen: "2e8b57",
                  seashell: "fff5ee",
                  sienna: "a0522d",
                  silver: "c0c0c0",
                  skyblue: "87ceeb",
                  slateblue: "6a5acd",
                  slategray: "708090",
                  slategrey: "708090",
                  snow: "fffafa",
                  springgreen: "00ff7f",
                  steelblue: "4682b4",
                  tan: "d2b48c",
                  teal: "008080",
                  thistle: "d8bfd8",
                  tomato: "ff6347",
                  turquoise: "40e0d0",
                  violet: "ee82ee",
                  wheat: "f5deb3",
                  white: "fff",
                  whitesmoke: "f5f5f5",
                  yellow: "ff0",
                  yellowgreen: "9acd32"
              };
              
              // Make it easy to access colors via `hexNames[hex]`
              var hexNames = tinycolor.hexNames = flip(names);
              
              
              // Utilities
              // ---------
              
              // `{ 'name1': 'val1' }` becomes `{ 'val1': 'name1' }`
              function flip(o) {
                  var flipped = {};
                  for(var i in o) {
                      if(o.hasOwnProperty(i)) {
                          flipped[o[i]] = i;
                      }
                  }
                  return flipped;
              }
              
              // Return a valid alpha value [0,1] with all invalid values being set to 1
              function boundAlpha(a) {
                  a = parseFloat(a);
                  
                  if(isNaN(a) || a < 0 || a > 1) {
                      a = 1;
                  }
                  
                  return a;
              }
              
              // Take input from [0, n] and return it as [0, 1]
              function bound01(n, max) {
                  if(isOnePointZero(n)) {
                      n = "100%";
                  }
                  
                  var processPercent = isPercentage(n);
                  n = mathMin(max, mathMax(0, parseFloat(n)));
                  
                  // Automatically convert percentage into number
                  if(processPercent) {
                      n = parseInt(n * max, 10) / 100;
                  }
                  
                  // Handle floating point rounding errors
                  if((math.abs(n - max) < 0.000001)) {
                      return 1;
                  }
                  
                  // Convert into [0, 1] range if it isn't already
                  return(n % max) / parseFloat(max);
              }
              
              // Force a number between 0 and 1
              function clamp01(val) {
                  return mathMin(1, mathMax(0, val));
              }
              
              // Parse an integer into hex
              function parseHex(val) {
                  return parseInt(val, 16);
              }
              
              // Need to handle 1.0 as 100%, since once it is a number, there is no difference between it and 1
              // <http://stackoverflow.com/questions/7422072/javascript-how-to-detect-number-as-a-decimal-including-1-0>
              function isOnePointZero(n) {
                  return typeof n == "string" && n.indexOf('.') != -1 && parseFloat(n) === 1;
              }
              
              // Check to see if string passed in is a percentage
              function isPercentage(n) {
                  return typeof n === "string" && n.indexOf('%') != -1;
              }
              
              // Force a hex value to have 2 characters
              function pad2(c) {
                  return c.length == 1 ? '0' + c : '' + c;
              }
              
              // Replace a decimal with it's percentage value
              function convertToPercentage(n) {
                  if(n <= 1) {
                      n = (n * 100) + "%";
                  }
                  
                  return n;
              }
              
              var matchers = (function() {
                  
                  // <http://www.w3.org/TR/css3-values/#integers>
                  var CSS_INTEGER = "[-\\+]?\\d+%?";
                  
                  // <http://www.w3.org/TR/css3-values/#number-value>
                  var CSS_NUMBER = "[-\\+]?\\d*\\.\\d+%?";
                  
                  // Allow positive/negative integer/number.  Don't capture the either/or, just the entire outcome.
                  var CSS_UNIT = "(?:" + CSS_NUMBER + ")|(?:" + CSS_INTEGER + ")";
                  
                  // Actual matching.
                  // Parentheses and commas are optional, but not required.
                  // Whitespace can take the place of commas or opening paren
                  var PERMISSIVE_MATCH3 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
                  var PERMISSIVE_MATCH4 = "[\\s|\\(]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")[,|\\s]+(" + CSS_UNIT + ")\\s*\\)?";
                  
                  return {
                      rgb: new RegExp("rgb" + PERMISSIVE_MATCH3),
                      rgba: new RegExp("rgba" + PERMISSIVE_MATCH4),
                      hsl: new RegExp("hsl" + PERMISSIVE_MATCH3),
                      hsla: new RegExp("hsla" + PERMISSIVE_MATCH4),
                      hsv: new RegExp("hsv" + PERMISSIVE_MATCH3),
                      hex3: /^([0-9a-fA-F]{1})([0-9a-fA-F]{1})([0-9a-fA-F]{1})$/,
                      hex6: /^([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$/
                  };
              })();
              
              // `stringInputToObject`
              // Permissive string parsing.  Take in a number of formats, and output an object
              // based on detected format.  Returns `{ r, g, b }` or `{ h, s, l }` or `{ h, s, v}`
              function stringInputToObject(color) {
                  
                  color = color.replace(trimLeft, '').replace(trimRight, '').toLowerCase();
                  var named = false;
                  if(names[color]) {
                      color = names[color];
                      named = true;
                  } else if(color == 'transparent') {
                      return {
                          r: 0,
                          g: 0,
                          b: 0,
                          a: 0,
                          format: "name"
                      };
                  }
                      
                      // Try to match string input using regular expressions.
                      // Keep most of the number bounding out of this function - don't worry about [0,1] or [0,100] or [0,360]
                      // Just return an object and let the conversion functions handle that.
                      // This way the result will be the same whether the tinycolor is initialized with string or object.
                      var match;
                  if((match = matchers.rgb.exec(color))) {
                      return {
                          r: match[1],
                          g: match[2],
                          b: match[3]
                      };
                  }
                  if((match = matchers.rgba.exec(color))) {
                      return {
                          r: match[1],
                          g: match[2],
                          b: match[3],
                          a: match[4]
                      };
                  }
                  if((match = matchers.hsl.exec(color))) {
                      return {
                          h: match[1],
                          s: match[2],
                          l: match[3]
                      };
                  }
                  if((match = matchers.hsla.exec(color))) {
                      return {
                          h: match[1],
                          s: match[2],
                          l: match[3],
                          a: match[4]
                      };
                  }
                  if((match = matchers.hsv.exec(color))) {
                      return {
                          h: match[1],
                          s: match[2],
                          v: match[3]
                      };
                  }
                  if((match = matchers.hex6.exec(color))) {
                      return {
                          r: parseHex(match[1]),
                          g: parseHex(match[2]),
                          b: parseHex(match[3]),
                          format: named ? "name" : "hex"
                      };
                  }
                  if((match = matchers.hex3.exec(color))) {
                      return {
                          r: parseHex(match[1] + '' + match[1]),
                          g: parseHex(match[2] + '' + match[2]),
                          b: parseHex(match[3] + '' + match[3]),
                          format: named ? "name" : "hex"
                      };
                  }
                  
                  return false;
              }
              
              // Expose tinycolor to window, does not need to run in non-browser context.
              window.tinycolor = tinycolor;
              
          })();
          
          
          $(function() {
              if($.fn.spectrum.load) {
                  $.fn.spectrum.processNativeColorInputs();
              }
          });
        })(window, jQuery);
        window.plugin.dispatchAssist.boot();
        
        $('head').append('<style>/* ================================================================== */\n/* Toolbars\n/* ================================================================== */\n\n.leaflet-draw-section {\n   position: relative;\n}\n\n.leaflet-draw-toolbar {\n margin-top: 12px;\n}\n\n.leaflet-draw-toolbar-top {\n   margin-top: 0;\n}\n\n.leaflet-draw-toolbar-notop a:first-child {\n  border-top-right-radius: 0;\n}\n\n.leaflet-draw-toolbar-nobottom a:last-child {\n   border-bottom-right-radius: 0;\n}\n\n.leaflet-draw-toolbar a {\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAAAeCAYAAABZs0CNAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtVJREFUeNrsmlFy2jAQhkWG93CD0hM0PUHhhdfADeAECScInAA4QblBeOYFcoK6Jyg5QdwTtLvp747GhNgmWttx/m/GY42s8Uqy/l1JlnOEEEIIIYQQQgghhBBCCCGEHNNiF5C6MxgMOnIbb7fbpcG7Z3K7S2XPxdasyHvaASv0J0lLJVpGDb6Ra4OGHho+eLpyG8rVQVasbbduNwat2u0i6wC7sbHNq3S+2Nzj2U6fS/qL5E0MqvBfOBhn1UQkNPZJBaSCCi0keedYbt9T2WtLQfmO4TWMnMat3BYnHk8tPDPs6mC+90TkPDGNxG5kICC113vhsX7fKdLaFzoGJlKHtVEUyhSZqZB8j2ExuE6IyFxQVQkpQ0RmYsJ3/AERafRZ4dENoqL279eQkUls7iCiA64EFewc40rp6/jSCFXXGUQ7oIi08f3AHZ0lIgdPNW7Ceg/TuUWOogspG3qa50/n+kn0UTuewIZwXKF4FpHY+pzhnHtSZlPC2uisaPQmIdVERE1jWLBsyKjU9aJs5KflWxyVCcgmQ0STVLQKujYK9aKLOooIFBFR1BAhdYzKnrNWOkoLlwbmfmeIKPKmeLXloqYicgWnEHFDhPSpQtt+H+506oPpjz+IH43rcOuLCBsLQ0unUYmQShRREnbzvvuhIUL6WaHzWKei3R2uzpnO7S0E2Z2rpZBKFpHDQnr1wSLSxqhsnv6OM9YMc8vvjTrMdBf0vYkot5CwFfxUlog8ljlF0og1EpzHNEfRqdH/s+WJhf0h8MaGz3We59jRrC3tAh+5BUGVJaJnLyk2V+71bcrY2e3qVCGmJXbJSv8hi/4eYebhn6gYGX3zvVw9/E96aXr+Dc57X/eTLLn+vVgf/8lh/5f7t/W6h2gekY7KEnUFbe66Co4IwXZywsE5gxMNqeXCqZMNvtiCCzn0fyRCCCGEEEIIIYQQQgghhBBCCCFG/BVgAMuWWtfqVjkMAAAAAElFTkSuQmCC);\n    background-repeat: no-repeat;\n}\n\n.leaflet-retina .leaflet-draw-toolbar a {\n background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAaQAAAA8CAYAAAApDs6vAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABV1JREFUeNrs3d1R20AUhmHBUIBLcCqI6UDccA0VYFcAqQBTAbgCQwVwzQ1OBTgVxCU4FZA9ydGMoki2flZmz+p9ZjTBxNaw7M+3KwkpSQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALo54lcAAMN1fn7+Ued9r6+vvefFCdUBAF4G9on7581t39zg/chvJIJAqkrrQ6SzxzJM3T9Lt0mjfHI/+4qmBgwijEbS993rxFooVY2xdVdQrJDCbZhLfSnBNHXfk0C6I5iiquNrt6VuGxf+e+O2VUwTEVfekbblK7dNCv+9lrLK5MuVdxtBOSc13rp1ZV3nPjfOhVHGZCh9tqMAG8VHPq2Lrw3NksqYDybfsyVjK9+xTjbSmh+Rep65Mm4M1/eFlnm0b5DWsr4YK5/U6a3bLmqUMTMrBo3bz1JDO//7OMsHV2x9t48+fBzggB7Dkr2KDGRv7r2ypQmsDczvDcIoq+93/azFMssA+1xzoJb3POtnLPXZdw2SxmEkYaarKhmUZ8nfQ/RBhhErJM8Desgz6ZphVDWDfrK0rB/iCkkD5bnjbi4trR52lFkG22ygnVS0+eDLqiuj94Z9Nh9GWZ/faPhs9fv32qcJI6uBVGdAD3Xg6hBGedKo7ywE09ACqeXAlVQM5KcWDt/prP9nSZmljc4L75XXtyVl/RLyOaWSQ2x/JofaF0vrLwuZkj6/zocS2jsJoGGUDegmlryewkjIoCcnQW9dmb/QLIOy9FC/ie5D9nVmoMzTOmGkE4q5nLwvhFJ2EcRDwGXMH0ZduXKcdejz2YVMl0M6ytHHZPL4k38xhNH/wYRw6jhNmp0z2ic1cp70qmR1MN8xMM213+7aR2jy/fauY5/f1t0HAg0kwqjUhiYZ9MDsw7WBcpdd2r3Pes8+Qrbu0OfNXsAgK5z8Kqfp62gCyXgYjXsKIwIpPKmRfaLbwLz1EUZ6XgqWAslyGKm6l8H2MlPDQY2N7PPQK6a27zGnYRhN6TKGAimCMMp+3r78okkiQCO9mq6qX897nKQRRgQSYbRDnycvVzRJBKCsP96WhVLFZd+xrPbrnjMijDw5yGXfEYWRHG9e6b3pUmOrL6DJxGhSEUrXyf4/jI1lchXNBQyskCIMo75XSTT04GyM7NO3xZ5BOtVt1HIfFhFG1gMp0jBK9OaojwMcqIa4UrCwT9/tW9pil1v/vFi+oWyhrrKNMDqA3g7ZxRpGhVXSlECK2iLxf35gYah9X3T4rHl1796AwFdIAwijbBbpc5XE7Cu8Ol57XtGsrLR//Tnb3PrngZUEPn2FtOf+SLEef81mkT4ueeWS7zDJYwV83Vx1ZrR9jxus8s2tjmQC3XZs4jEyBlZIAwmjbJXk6xDMiuYYbB37CBJzD+rTOxjMGpbRypWi+Z+zy/Oq0op9IoRAKrnfUewnAx88NUQac7gDs5zgv2xZR/KZS2tPUs2VfVUzlGbGnoScrw+5nP0me9heg9XRTfLv32C90Fu6OTlAg476eLLMCF3DXCTlfxxYd2W04bh7+KHk6lnqaFCPMNeyP7qyf3Vf3lRNyiw9ZFIVD7fLg/Xu9VEabSeU3PG7I293bq06h2ThiaCeyi8PNBvveIsMZjIw/dCv15FcGjs4etHOtQZTsc43GkSL2CYZ+njyZcnK6NFwPfq4UbKp0xIhPw/pJIHPGddSByTZvmdfGzuUgXqr/tkAyy0rpU0ulGaW27bUoyvPqR7daHNxkgSRHKa7Y3IJAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjLbwEGABdIk04CWmrYAAAAAElFTkSuQmCC);\n    background-size: 210px 30px;\n}\n\n.leaflet-draw a {\n  display: block;\n   text-align: center;\n   text-decoration: none;\n}\n\n/* ================================================================== */\n/* Toolbar actions menu\n/* ================================================================== */\n\n.leaflet-draw-actions {\n   display: none;\n    list-style: none;\n margin: 0;\n    padding: 0;\n   position: absolute;\n   left: 26px; /* leaflet-draw-toolbar.left + leaflet-draw-toolbar.width */\n  top: 0;\n   white-space: nowrap;\n}\n\n.leaflet-right .leaflet-draw-actions {\n right:26px;\n   left:auto;\n}\n\n.leaflet-draw-actions li {\n   display: inline-block;\n}\n\n.leaflet-draw-actions li:first-child a {\n border-left: none;\n}\n\n.leaflet-draw-actions li:last-child a {\n  -webkit-border-radius: 0 4px 4px 0;\n           border-radius: 0 4px 4px 0;\n}\n\n.leaflet-right .leaflet-draw-actions li:last-child a {\n  -webkit-border-radius: 0;\n         border-radius: 0;\n}\n\n.leaflet-right .leaflet-draw-actions li:first-child a {\n   -webkit-border-radius: 4px 0 0 4px;\n           border-radius: 4px 0 0 4px;\n}\n\n.leaflet-draw-actions a {\n   background-color: #919187;\n    border-left: 1px solid #AAA;\n  color: #FFF;\n  font: 11px/19px "Helvetica Neue", Arial, Helvetica, sans-serif;\n   line-height: 28px;\n    text-decoration: none;\n    padding-left: 10px;\n   padding-right: 10px;\n  height: 28px;\n}\n\n.leaflet-draw-actions-bottom {\n    margin-top: 0;\n}\n\n.leaflet-draw-actions-top {\n  margin-top: 1px;\n}\n\n.leaflet-draw-actions-top a,\n.leaflet-draw-actions-bottom a {\n height: 27px;\n line-height: 27px;\n}\n\n.leaflet-draw-actions a:hover {\n  background-color: #A0A098;\n}\n\n.leaflet-draw-actions-top.leaflet-draw-actions-bottom a {\n    height: 26px;\n line-height: 26px;\n}\n\n/* ================================================================== */\n/* Draw toolbar\n/* ================================================================== */\n\n.leaflet-draw-toolbar .leaflet-draw-draw-polyline {\n   background-position: -2px -2px;\n}\n\n.leaflet-draw-toolbar .leaflet-draw-draw-polygon {\n  background-position: -31px -2px;\n}\n\n.leaflet-draw-toolbar .leaflet-draw-draw-rectangle {\n   background-position: -62px -2px;\n}\n\n.leaflet-draw-toolbar .leaflet-draw-draw-circle {\n  background-position: -92px -2px;\n}\n\n.leaflet-draw-toolbar .leaflet-draw-draw-marker {\n  background-position: -122px -2px;\n}\n\n/* ================================================================== */\n/* Edit toolbar\n/* ================================================================== */\n\n.leaflet-draw-toolbar .leaflet-draw-edit-edit {\n    background-position: -152px -2px;\n}\n\n.leaflet-draw-toolbar .leaflet-draw-edit-remove {\n background-position: -182px -2px;\n}\n\n/* ================================================================== */\n/* Drawing styles\n/* ================================================================== */\n\n.leaflet-mouse-marker {\n  background-color: #fff;\n   cursor: crosshair;\n}\n\n.leaflet-draw-tooltip {\n  background: rgb(54, 54, 54);\n  background: rgba(0, 0, 0, 0.5);\n   border: 1px solid transparent;\n    -webkit-border-radius: 4px;\n           border-radius: 4px;\n   color: #fff;\n  font: 12px/18px "Helvetica Neue", Arial, Helvetica, sans-serif;\n   margin-left: 20px;\n    margin-top: -21px;\n    padding: 4px 8px;\n position: absolute;\n   white-space: nowrap;\n  z-index: 6;\n}\n\n.leaflet-draw-tooltip:before {\n  border-right: 6px solid black;\n    border-right-color: rgba(0, 0, 0, 0.5);\n   border-top: 6px solid transparent;\n    border-bottom: 6px solid transparent;\n content: "";\n  position: absolute;\n   top: 7px;\n left: -7px;\n}\n\n.leaflet-error-draw-tooltip {\n   background-color: #F2DEDE;\n    border: 1px solid #E6B6BD;\n    color: #B94A48;\n}\n\n.leaflet-error-draw-tooltip:before {\n    border-right-color: #E6B6BD;\n}\n\n.leaflet-draw-tooltip-single {\n margin-top: -12px\n}\n\n.leaflet-draw-tooltip-subtext {\n   color: #f8d5e4;\n}\n\n.leaflet-draw-guide-dash {\n  font-size: 1%;\n    opacity: 0.6;\n position: absolute;\n   width: 5px;\n   height: 5px;\n}\n\n/* ================================================================== */\n/* Edit styles\n/* ================================================================== */\n\n.leaflet-edit-marker-selected {\n  background: rgba(254, 87, 161, 0.1);\n  border: 4px dashed rgba(254, 87, 161, 0.6);\n   -webkit-border-radius: 4px;\n           border-radius: 4px;\n}\n\n.leaflet-edit-move {\n    cursor: move;\n}\n\n.leaflet-edit-resize {\n    cursor: pointer;\n}\n</style>');
        $('head').append('<style>/***\nSpectrum Colorpicker v1.2.0\nhttps://github.com/bgrins/spectrum\nAuthor: Brian Grinstead\nLicense: MIT\n***/\n\n.sp-container {\n    position:absolute;\n    top:0;\n    left:0;\n    display:inline-block;\n    *display: inline;\n    *zoom: 1;\n    /* https://github.com/bgrins/spectrum/issues/40 */\n    z-index: 9999994;\n    overflow: hidden;\n}\n.sp-container.sp-flat {\n    position: relative;\n}\n\n/* http://ansciath.tumblr.com/post/7347495869/css-aspect-ratio */\n.sp-top {\n  position:relative;\n  width: 100%;\n  display:inline-block;\n}\n.sp-top-inner {\n   position:absolute;\n   top:0;\n   left:0;\n   bottom:0;\n   right:0;\n}\n.sp-color {\n    position: absolute;\n    top:0;\n    left:0;\n    bottom:0;\n    right:20%;\n}\n.sp-hue {\n    position: absolute;\n    top:0;\n    right:0;\n    bottom:0;\n    left:84%;\n    height: 100%;\n}\n\n.sp-clear-enabled .sp-hue {\n    top:33px;\n    height: 77.5%;\n}\n\n.sp-fill {\n    padding-top: 80%;\n}\n.sp-sat, .sp-val {\n    position: absolute;\n    top:0;\n    left:0;\n    right:0;\n    bottom:0;\n}\n\n.sp-alpha-enabled .sp-top {\n    margin-bottom: 18px;\n}\n.sp-alpha-enabled .sp-alpha {\n    display: block;\n}\n.sp-alpha-handle {\n    position:absolute;\n    top:-4px;\n    bottom: -4px;\n    width: 6px;\n    left: 50%;\n    cursor: pointer;\n    border: 1px solid black;\n    background: white;\n    opacity: .8;\n}\n.sp-alpha {\n    display: none;\n    position: absolute;\n    bottom: -14px;\n    right: 0;\n    left: 0;\n    height: 8px;\n}\n.sp-alpha-inner {\n    border: solid 1px #333;\n}\n\n.sp-clear {\n    display: none;\n}\n\n.sp-clear.sp-clear-display {\n    background-position: center;\n}\n\n.sp-clear-enabled .sp-clear {\n    display: block;\n    position:absolute;\n    top:0px;\n    right:0;\n    bottom:0;\n    left:84%;\n    height: 28px;\n}\n\n/* Don\'t allow text selection */\n.sp-container, .sp-replacer, .sp-preview, .sp-dragger, .sp-slider, .sp-alpha, .sp-clear, .sp-alpha-handle, .sp-container.sp-dragging .sp-input, .sp-container button  {\n    -webkit-user-select:none;\n    -moz-user-select: -moz-none;\n    -o-user-select:none;\n    user-select: none;\n}\n\n.sp-container.sp-input-disabled .sp-input-container {\n    display: none;\n}\n.sp-container.sp-buttons-disabled .sp-button-container {\n    display: none;\n}\n.sp-palette-only .sp-picker-container {\n    display: none;\n}\n.sp-palette-disabled .sp-palette-container {\n    display: none;\n}\n\n.sp-initial-disabled .sp-initial {\n    display: none;\n}\n\n\n/* Gradients for hue, saturation and value instead of images.  Not pretty... but it works */\n.sp-sat {\n    background-image: -webkit-gradient(linear,  0 0, 100% 0, from(#FFF), to(rgba(204, 154, 129, 0)));\n    background-image: -webkit-linear-gradient(left, #FFF, rgba(204, 154, 129, 0));\n    background-image: -moz-linear-gradient(left, #fff, rgba(204, 154, 129, 0));\n    background-image: -o-linear-gradient(left, #fff, rgba(204, 154, 129, 0));\n    background-image: -ms-linear-gradient(left, #fff, rgba(204, 154, 129, 0));\n    background-image: linear-gradient(to right, #fff, rgba(204, 154, 129, 0));\n    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(GradientType = 1, startColorstr=#FFFFFFFF, endColorstr=#00CC9A81)";\n    filter : progid:DXImageTransform.Microsoft.gradient(GradientType = 1, startColorstr=\'#FFFFFFFF\', endColorstr=\'#00CC9A81\');\n}\n.sp-val {\n    background-image: -webkit-gradient(linear, 0 100%, 0 0, from(#000000), to(rgba(204, 154, 129, 0)));\n    background-image: -webkit-linear-gradient(bottom, #000000, rgba(204, 154, 129, 0));\n    background-image: -moz-linear-gradient(bottom, #000, rgba(204, 154, 129, 0));\n    background-image: -o-linear-gradient(bottom, #000, rgba(204, 154, 129, 0));\n    background-image: -ms-linear-gradient(bottom, #000, rgba(204, 154, 129, 0));\n    background-image: linear-gradient(to top, #000, rgba(204, 154, 129, 0));\n    -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#00CC9A81, endColorstr=#FF000000)";\n    filter : progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#00CC9A81\', endColorstr=\'#FF000000\');\n}\n\n.sp-hue {\n    background: -moz-linear-gradient(top, #ff0000 0%, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);\n    background: -ms-linear-gradient(top, #ff0000 0%, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);\n    background: -o-linear-gradient(top, #ff0000 0%, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);\n    background: -webkit-gradient(linear, left top, left bottom, from(#ff0000), color-stop(0.17, #ffff00), color-stop(0.33, #00ff00), color-stop(0.5, #00ffff), color-stop(0.67, #0000ff), color-stop(0.83, #ff00ff), to(#ff0000));\n    background: -webkit-linear-gradient(top, #ff0000 0%, #ffff00 17%, #00ff00 33%, #00ffff 50%, #0000ff 67%, #ff00ff 83%, #ff0000 100%);\n}\n\n/* IE filters do not support multiple color stops.\n   Generate 6 divs, line them up, and do two color gradients for each.\n   Yes, really.\n */\n.sp-1 {\n    height:17%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ff0000\', endColorstr=\'#ffff00\');\n}\n.sp-2 {\n    height:16%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ffff00\', endColorstr=\'#00ff00\');\n}\n.sp-3 {\n    height:17%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#00ff00\', endColorstr=\'#00ffff\');\n}\n.sp-4 {\n    height:17%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#00ffff\', endColorstr=\'#0000ff\');\n}\n.sp-5 {\n    height:16%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#0000ff\', endColorstr=\'#ff00ff\');\n}\n.sp-6 {\n    height:17%;\n    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#ff00ff\', endColorstr=\'#ff0000\');\n}\n\n.sp-hidden {\n    display: none !important;\n}\n\n/* Clearfix hack */\n.sp-cf:before, .sp-cf:after { content: ""; display: table; }\n.sp-cf:after { clear: both; }\n.sp-cf { *zoom: 1; }\n\n/* Mobile devices, make hue slider bigger so it is easier to slide */\n@media (max-device-width: 480px) {\n    .sp-color { right: 40%; }\n    .sp-hue { left: 63%; }\n    .sp-fill { padding-top: 60%; }\n}\n.sp-dragger {\n   border-radius: 5px;\n   height: 5px;\n   width: 5px;\n   border: 1px solid #fff;\n   background: #000;\n   cursor: pointer;\n   position:absolute;\n   top:0;\n   left: 0;\n}\n.sp-slider {\n    position: absolute;\n    top:0;\n    cursor:pointer;\n    height: 3px;\n    left: -1px;\n    right: -1px;\n    border: 1px solid #000;\n    background: white;\n    opacity: .8;\n}\n\n/*\nTheme authors:\nHere are the basic themeable display options (colors, fonts, global widths).\nSee http://bgrins.github.io/spectrum/themes/ for instructions.\n*/\n\n.sp-container {\n    border-radius: 0;\n    background-color: #ECECEC;\n    border: solid 1px #f0c49B;\n    padding: 0;\n}\n.sp-container, .sp-container button, .sp-container input, .sp-color, .sp-hue, .sp-clear\n{\n    font: normal 12px "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", Geneva, Verdana, sans-serif;\n    -webkit-box-sizing: border-box;\n    -moz-box-sizing: border-box;\n    -ms-box-sizing: border-box;\n    box-sizing: border-box;\n}\n.sp-top\n{\n    margin-bottom: 3px;\n}\n.sp-color, .sp-hue, .sp-clear\n{\n    border: solid 1px #666;\n}\n\n/* Input */\n.sp-input-container {\n    float:right;\n    width: 100px;\n    margin-bottom: 4px;\n}\n.sp-initial-disabled  .sp-input-container {\n    width: 100%;\n}\n.sp-input {\n   font-size: 12px !important;\n   border: 1px inset;\n   padding: 4px 5px;\n   margin: 0;\n   width: 100%;\n   background:transparent;\n   border-radius: 3px;\n   color: #222;\n}\n.sp-input:focus  {\n    border: 1px solid orange;\n}\n.sp-input.sp-validation-error\n{\n    border: 1px solid red;\n    background: #fdd;\n}\n.sp-picker-container , .sp-palette-container\n{\n    float:left;\n    position: relative;\n    padding: 10px;\n    padding-bottom: 300px;\n    margin-bottom: -290px;\n}\n.sp-picker-container\n{\n    width: 172px;\n    border-left: solid 1px #fff;\n}\n\n/* Palettes */\n.sp-palette-container\n{\n    border-right: solid 1px #ccc;\n}\n\n.sp-palette .sp-thumb-el {\n    display: block;\n    position:relative;\n    float:left;\n    width: 24px;\n    height: 15px;\n    margin: 3px;\n    cursor: pointer;\n    border:solid 2px transparent;\n}\n.sp-palette .sp-thumb-el:hover, .sp-palette .sp-thumb-el.sp-thumb-active {\n    border-color: orange;\n}\n.sp-thumb-el\n{\n    position:relative;\n}\n\n/* Initial */\n.sp-initial\n{\n    float: left;\n    border: solid 1px #333;\n}\n.sp-initial span {\n    width: 30px;\n    height: 25px;\n    border:none;\n    display:block;\n    float:left;\n    margin:0;\n}\n\n.sp-initial .sp-clear-display {\n    background-position: center;\n}\n\n/* Buttons */\n.sp-button-container {\n    float: right;\n}\n\n/* Replacer (the little preview div that shows up instead of the <input>) */\n.sp-replacer {\n    margin:0;\n    overflow:hidden;\n    cursor:pointer;\n    padding: 4px;\n    display:inline-block;\n    *zoom: 1;\n    *display: inline;\n    border: solid 1px #91765d;\n    background: #eee;\n    color: #333;\n    vertical-align: middle;\n}\n.sp-replacer:hover, .sp-replacer.sp-active {\n    border-color: #F0C49B;\n    color: #111;\n}\n.sp-replacer.sp-disabled {\n    cursor:default;\n    border-color: silver;\n    color: silver;\n}\n.sp-dd {\n    padding: 2px 0;\n    height: 16px;\n    line-height: 16px;\n    float:left;\n    font-size:10px;\n}\n.sp-preview\n{\n    position:relative;\n    width:25px;\n    height: 20px;\n    border: solid 1px #222;\n    margin-right: 5px;\n    float:left;\n    z-index: 0;\n}\n\n.sp-palette\n{\n    *width: 220px;\n    max-width: 220px;\n}\n.sp-palette .sp-thumb-el\n{\n    width:16px;\n    height: 16px;\n    margin:2px 1px;\n    border: solid 1px #d0d0d0;\n}\n\n.sp-container\n{\n    padding-bottom:0;\n}\n\n\n/* Buttons: http://hellohappy.org/css3-buttons/ */\n.sp-container button {\n  background-color: #eeeeee;\n  background-image: -webkit-linear-gradient(top, #eeeeee, #cccccc);\n  background-image: -moz-linear-gradient(top, #eeeeee, #cccccc);\n  background-image: -ms-linear-gradient(top, #eeeeee, #cccccc);\n  background-image: -o-linear-gradient(top, #eeeeee, #cccccc);\n  background-image: linear-gradient(to bottom, #eeeeee, #cccccc);\n  border: 1px solid #ccc;\n  border-bottom: 1px solid #bbb;\n  border-radius: 3px;\n  color: #333;\n  font-size: 14px;\n  line-height: 1;\n  padding: 5px 4px;\n  text-align: center;\n  text-shadow: 0 1px 0 #eee;\n  vertical-align: middle;\n}\n.sp-container button:hover {\n    background-color: #dddddd;\n    background-image: -webkit-linear-gradient(top, #dddddd, #bbbbbb);\n    background-image: -moz-linear-gradient(top, #dddddd, #bbbbbb);\n    background-image: -ms-linear-gradient(top, #dddddd, #bbbbbb);\n    background-image: -o-linear-gradient(top, #dddddd, #bbbbbb);\n    background-image: linear-gradient(to bottom, #dddddd, #bbbbbb);\n    border: 1px solid #bbb;\n    border-bottom: 1px solid #999;\n    cursor: pointer;\n    text-shadow: 0 1px 0 #ddd;\n}\n.sp-container button:active {\n    border: 1px solid #aaa;\n    border-bottom: 1px solid #888;\n    -webkit-box-shadow: inset 0 0 5px 2px #aaaaaa, 0 1px 0 0 #eeeeee;\n    -moz-box-shadow: inset 0 0 5px 2px #aaaaaa, 0 1px 0 0 #eeeeee;\n    -ms-box-shadow: inset 0 0 5px 2px #aaaaaa, 0 1px 0 0 #eeeeee;\n    -o-box-shadow: inset 0 0 5px 2px #aaaaaa, 0 1px 0 0 #eeeeee;\n    box-shadow: inset 0 0 5px 2px #aaaaaa, 0 1px 0 0 #eeeeee;\n}\n.sp-cancel\n{\n    font-size: 11px;\n    color: #d93f3f !important;\n    margin:0;\n    padding:2px;\n    margin-right: 5px;\n    vertical-align: middle;\n    text-decoration:none;\n\n}\n.sp-cancel:hover\n{\n    color: #d93f3f !important;\n    text-decoration: underline;\n}\n\n\n.sp-palette span:hover, .sp-palette span.sp-thumb-active\n{\n    border-color: #000;\n}\n\n.sp-preview, .sp-alpha, .sp-thumb-el\n{\n    position:relative;\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAIAAADZF8uwAAAAGUlEQVQYV2M4gwH+YwCGIasIUwhT25BVBADtzYNYrHvv4gAAAABJRU5ErkJggg==);\n}\n.sp-preview-inner, .sp-alpha-inner, .sp-thumb-inner\n{\n    display:block;\n    position:absolute;\n    top:0;left:0;bottom:0;right:0;\n}\n\n.sp-palette .sp-thumb-inner\n{\n    background-position: 50% 50%;\n    background-repeat: no-repeat;\n}\n\n.sp-palette .sp-thumb-light.sp-thumb-active .sp-thumb-inner\n{\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAIVJREFUeNpiYBhsgJFMffxAXABlN5JruT4Q3wfi/0DsT64h8UD8HmpIPCWG/KemIfOJCUB+Aoacx6EGBZyHBqI+WsDCwuQ9mhxeg2A210Ntfo8klk9sOMijaURm7yc1UP2RNCMbKE9ODK1HM6iegYLkfx8pligC9lCD7KmRof0ZhjQACDAAceovrtpVBRkAAAAASUVORK5CYII=);\n}\n\n.sp-palette .sp-thumb-dark.sp-thumb-active .sp-thumb-inner\n{\n    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAAAMdJREFUOE+tkgsNwzAMRMugEAahEAahEAZhEAqlEAZhEAohEAYh81X2dIm8fKpEspLGvudPOsUYpxE2BIJCroJmEW9qJ+MKaBFhEMNabSy9oIcIPwrB+afvAUFoK4H0tMaQ3XtlrggDhOVVMuT4E5MMG0FBbCEYzjYT7OxLEvIHQLY2zWwQ3D+9luyOQTfKDiFD3iUIfPk8VqrKjgAiSfGFPecrg6HN6m/iBcwiDAo7WiBeawa+Kwh7tZoSCGLMqwlSAzVDhoK+6vH4G0P5wdkAAAAASUVORK5CYII=);\n}\n\n.sp-clear-display {\n    background-repeat:no-repeat;\n    background-position: center;\n    background-image: url(data:image/gif;base64,R0lGODlhFAAUAPcAAAAAAJmZmZ2dnZ6enqKioqOjo6SkpKWlpaampqenp6ioqKmpqaqqqqurq/Hx8fLy8vT09PX19ff39/j4+Pn5+fr6+vv7+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAP8ALAAAAAAUABQAAAihAP9FoPCvoMGDBy08+EdhQAIJCCMybCDAAYUEARBAlFiQQoMABQhKUJBxY0SPICEYHBnggEmDKAuoPMjS5cGYMxHW3IiT478JJA8M/CjTZ0GgLRekNGpwAsYABHIypcAgQMsITDtWJYBR6NSqMico9cqR6tKfY7GeBCuVwlipDNmefAtTrkSzB1RaIAoXodsABiZAEFB06gIBWC1mLVgBa0AAOw==);\n}\n</style>');
    };
                                          
    var setup = window.plugin.dispatchAssist.loadExternals;
                                          
    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
}; // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if(typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = {
    version: GM_info.script.version,
    name: GM_info.script.name,
    description: GM_info.script.description
};
script.appendChild(document.createTextNode('(' + wrapper + ')(' + JSON.stringify(info) + ');'));
(document.body || document.head || document.documentElement).appendChild(script);
