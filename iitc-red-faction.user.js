// ==UserScript==
// @id             iitc-red-faction
// @name           IITC Red Faction
// @category       Tweaks
// @version        1.02
// @description    This plugin makes your name red
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-red-faction.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-red-faction.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

// PLUGIN START ////////////////////////////////////////////////////////

window.plugin.redFaction = function() {};

window.plugin.redFaction.setup = function() {
  $("#name > span").attr('style', 'color:red !important;font-weight: bold');
  $('#name').append(' 💀');
};

var setup = window.plugin.redFaction.setup;

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


