// ==UserScript==
// @id             set-oldest
// @name           Set Oldest IITC Plugin
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-set-oldest.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-set-oldest.user.js
// @version        1.1
// @description    Make it easier to scroll chat back to a date
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};


// PLUGIN START ////////////////////////////////////////////////////////

window.plugin.setoldest = function() {};

window.plugin.setoldest.setup = function() {
    $('#toolbox').append('<a tabindex="0" onclick="window.plugin.setoldest.openDialog()">Set chat scrollback point</a>');
}

window.plugin.setoldest.openDialog = function() {
    dialog({
            html: 'Date/Time: <form class="setoldest"><input class="dt" type="text"/><input type="submit"/></form>',
            dialogClass: 'ui-dialog-setOldest',
            title: 'Set chat scrollback point',
            id: 'setoldest'
        });
    $('.setoldest').submit(function(e) {
        e.preventDefault();
        var dt = $('input.dt', $(e.target)).val();
        var d = new Date(dt);
        if (d != 'Invalid Date') {
            var ts = d.getTime();
            chat._public.oldestTimestamp = ts;
            $('#dialog-setoldest').html('Interpreted as ' + d + '. Set successfully. Scroll up in comms to now retrieve message before the stated point in time.');
        } else {
            $('#dialog-setoldest').append('<div>Invalid Date. Try again?</div>');
        }
    });
}

var setup = window.plugin.setoldest.setup;

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
