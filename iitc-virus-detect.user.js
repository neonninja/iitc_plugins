// ==UserScript==
// @id             iitc-virus-detect
// @name           IITC Virus Detection
// @category       Tweaks
// @version        1.04
// @description    This plugin detects when ADA's and JARVIS's are used
// @updateURL      https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-virus-detect.user.js
// @downloadURL    https://gitlab.com/neonninja/iitc_plugins/raw/master/iitc-virus-detect.user.js
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==


function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};

    // PLUGIN START ////////////////////////////////////////////////////////

    window.plugin.virus = function() {};

    window.plugin.virus.setup = function() {
        window.plugin.virus.hash = {};
        window.plugin.virus.processedGuids = [];
        $('#toolbox').append('<a tabindex="0" onclick="window.plugin.virus.openDialog()">Show recent viruses</a>');
        addHook('publicChatDataAvailable', function(data) {
            var shouldUpdate = false;
            for (var i in data.result) {
                var m = data.result[i];
                var guid = m[0];
                if (window.plugin.virus.processedGuids.indexOf(guid) > -1) continue;
                window.plugin.virus.processedGuids.push(guid);
                var ts = m[1];
                var p = m[2].plext;
                if (p.plextType == 'SYSTEM_BROADCAST' && p.markup[1][0] == 'TEXT' && p.markup[1][1].plain == ' destroyed a Resonator on ') {
                    var player = p.markup[0][1];
                    var portal = p.markup[2][1];
                    var id = ts + '_' + portal.latE6 + '_' + portal.lngE6 + '_' + player.plain;
                    if (window.plugin.virus.hash[id]) {
                        window.plugin.virus.hash[id].count += 1;
                        shouldUpdate = true;
                    } else {
                        window.plugin.virus.hash[id] = {portal: portal, player: player, count: 1, ts: ts};
                    }
                }
            }
            if (shouldUpdate && $('.ui-dialog-virus').length) {
                window.plugin.virus.openDialog();
            }
        });
    };

    window.plugin.virus.openDialog = function() {
        html = '<table><tr><th>Time</th><th>Agent</th><th>Portal</th><th>Virus Type</th>';
        if (window.plugin.dispatchAssist) html += '<th>Codename</th>';
        html += '</tr>';
        for (var id in window.plugin.virus.hash) {
            var data = window.plugin.virus.hash[id];
            if (data.count > 1) {
                var team = data.player.team === 'RESISTANCE' ? TEAM_RES : TEAM_ENL;
                var color = COLORS[team];
                var latlng = [data.portal.latE6/1E6, data.portal.lngE6/1E6];
                var perma = '/intel?ll='+latlng[0]+','+latlng[1]+'&z=17&pll='+latlng[0]+','+latlng[1];
                var js = 'window.selectPortalByLatLng('+latlng[0]+', '+latlng[1]+');return false';

                var portal = '<a onclick="' + js + '"' +
                    ' title="' + data.portal.address + '"' +
                    ' href="' + perma +'" class="help">' +
                    data.portal.name +
                    '</a>';
                var virusType;
                var codeName = '';
                if (window.plugin.dispatchAssist) {
                    codeName = '<td></td>';
                    for (var i in window.plugin.dispatchAssist.portalDispatchInfo) {
                        var d = window.plugin.dispatchAssist.portalDispatchInfo[i];
                        if (parseFloat(d.lat) == latlng[0] && parseFloat(d.lng) == latlng[1] && d.name == data.portal.name) {
                            codeName = '<td>' + d.cluster + ' - ' + d.codename + '</td>';
                        }
                    }
                }
                if (data.portal.team == 'RESISTANCE') {
                    virusType = '<span style="color:' + COLORS[TEAM_RES] + '">ADA Refactor</span>';
                } else {
                    virusType = '<span style="color:' + COLORS[TEAM_ENL] + '">JARVIS Virus</span>';
                }
                html += '<tr><td>' + new Date(parseInt(data.ts)) + '</td><td style="color:' + color + '">' +
                    data.player.plain + '</td><td>' + portal + '</td><td>' + virusType + '</td>' + codeName + '</tr>';
            }
        }
        dialog({
            html: html,
            dialogClass: 'ui-dialog-virus',
            title: 'Recent viruses',
            id: 'virus',
            width: 450,
            minHeight: 150,
            //position: { my: "left+30 top+20", at: "left+30 top+20", of: window },
        });
    };

    var setup = window.plugin.virus.setup;

    // PLUGIN END //////////////////////////////////////////////////////////


    setup.info = plugin_info; //add the script info data to the function as a property
    if(!window.bootPlugins) window.bootPlugins = [];
    window.bootPlugins.push(setup);
    // if IITC has already booted, immediately run the 'setup' function
    if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


